const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
var cors = require('cors');
const fs = require('fs');
const axios = require('axios');
const externalModules = require('./externalModulesBackendFactory.js');
var app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());
require('dotenv').config()
require('./server/routers')(app);

app.use(express.static(path.join(__dirname, 'build')));
Object.keys(externalModules).forEach(item=>{ app.use('/', externalModules[item]) });

app.get('/ping', function (req, res) {
 return res.send('pong');
});


app.get('/sendXML', async function (req, res) {
  const data = fs.readFileSync('test-report.xml', 'utf8');
  const response = await axios.post('https://conserveapi.corebooks.com/form/xml', data, {
    headers: {
      'Content-Type': 'application/xml'
    }
  });
  res.status(response.status).send(response.data);
});



app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

module.exports = app;