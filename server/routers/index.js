const fs = require('fs');
const ProccesRoutes = (app)=>{
    fs.readdirSync(__dirname).forEach(function(file) {
        if (file == "index.js") return;
        require(`./${file}`)(app);
    })
}

module.exports = ProccesRoutes;