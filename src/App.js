import {
  Router,
  Switch,
  Route
} from "react-router-dom";
import React from "react";
import './App.css';
import HomeLayout from './Layouts'

import OverlaySwapProvider from "Components/OverlaySwapProvider";

const SwitchMemo = React.memo(()=>{
  
  return(<Route path="/" component={HomeLayout} />)
})

function App() {
  return (
    <Switch>
      <OverlaySwapProvider>
        
        <SwitchMemo />
        
      </OverlaySwapProvider>     
    </Switch> 
  );
}

export default App;
