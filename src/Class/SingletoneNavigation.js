class SingletoneNavigation{
    constructor(){
        this.transition = { };
        this.easing = { type: null, time: null };

    }

    static CreateInstance(){
        this.instance = new SingletoneNavigation();
    }

    static setTransitionInstance(transition, ComponentName){
        const cssClass = [ transition?.type, transition?.direction ].filter(item=>item).join('-').toLowerCase().replace(/_/g, '-');
        this.instance.transition = {};
        this.instance.transition[ComponentName] = { ...transition, cssClass  }
    }

    static getInstance(){
        return this.instance;
    }

    static getTransitionInstance(){
        return this.getInstance().transition;
    }

}

export default SingletoneNavigation