import React from "react";
import "./HomeLayout.css";
import LoadingComponent from "Components/LoadingComponent";
import { useAuth0 } from "@auth0/auth0-react";
import CorebooksAnimation from "../CorebooksAnimation";

const Test = () => {
  return (
    <p
      className="header-test"
      style={{
        backgroundColor: "red",
      }}
    >
      A
    </p>
  );
};

const HomeLayout = (props) => {
  const { isLoading, isAuthenticated, logout } = useAuth0();
  const [animate, setAnimate] = React.useState(false);

  if (isLoading) {
    return <LoadingComponent />;
  }
  return (
    <div className="main-page">
      {!isLoading && isAuthenticated ? (
        <p>Header test</p>
      ) : (
        <>
          <div
            style={{ width: 400, height: 400, marginLeft: 150, marginTop: 150 }}
          >
            <CorebooksAnimation
              animationType="SLIDE_IN"
              direction="BOTTOM"
              active={animate}
              easing="ease-in-out"
              duration="3s"
            >
              <Test />
              <p
                className={`header-test`}
                style={{
                  backgroundColor: "blue",
                }}
              >
                B
              </p>
            </CorebooksAnimation>
          </div>
          <button
            style={{ marginLeft: 200 }}
            onClick={() => setAnimate(!animate)}
          >
            Animar
          </button>
        </>
      )}
      <div className="main-body">{props.children}</div>
    </div>
  );
};

export default HomeLayout;
