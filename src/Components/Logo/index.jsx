import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Logo.css'





const Logo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['logo']?.animationClass || {}}>

    <div id="id_twoo_five" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } logo ${ props.cssClass } ${ transaction['logo']?.type ? transaction['logo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['logo']?.duration, transitionTimingFunction: transaction['logo']?.timingFunction }, ...props.style }} onClick={ props.LogoonClick } onMouseEnter={ props.LogoonMouseEnter } onMouseOver={ props.LogoonMouseOver } onKeyPress={ props.LogoonKeyPress } onDrag={ props.LogoonDrag } onMouseLeave={ props.LogoonMouseLeave } onMouseUp={ props.LogoonMouseUp } onMouseDown={ props.LogoonMouseDown } onKeyDown={ props.LogoonKeyDown } onChange={ props.LogoonChange } ondelay={ props.Logoondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangletwooeight']?.animationClass || {}}>
          <div id="id_threefourtwoo_oneeighttwooone" className={` rectangle rectangletwooeight ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangletwooeight']?.type ? transaction['rectangletwooeight']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangletwooeightStyle , transitionDuration: transaction['rectangletwooeight']?.duration, transitionTimingFunction: transaction['rectangletwooeight']?.timingFunction }} onClick={ props.RectangletwooeightonClick } onMouseEnter={ props.RectangletwooeightonMouseEnter } onMouseOver={ props.RectangletwooeightonMouseOver } onKeyPress={ props.RectangletwooeightonKeyPress } onDrag={ props.RectangletwooeightonDrag } onMouseLeave={ props.RectangletwooeightonMouseLeave } onMouseUp={ props.RectangletwooeightonMouseUp } onMouseDown={ props.RectangletwooeightonMouseDown } onKeyDown={ props.RectangletwooeightonKeyDown } onChange={ props.RectangletwooeightonChange } ondelay={ props.Rectangletwooeightondelay }></div>
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['logo_concert_vtwoocolorone']?.animationClass || {}}>

          <div id="id_threefourtwoo_oneeighttwootwoo" className={` frame logo_concert_vtwoocolorone ${ props.onClick ? 'cursor' : '' } ${ transaction['logo_concert_vtwoocolorone']?.type ? transaction['logo_concert_vtwoocolorone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LOGO_CONCERT_VtwooCOLORoneStyle , transitionDuration: transaction['logo_concert_vtwoocolorone']?.duration, transitionTimingFunction: transaction['logo_concert_vtwoocolorone']?.timingFunction } } onClick={ props.LOGO_CONCERT_VtwooCOLORoneonClick } onMouseEnter={ props.LOGO_CONCERT_VtwooCOLORoneonMouseEnter } onMouseOver={ props.LOGO_CONCERT_VtwooCOLORoneonMouseOver } onKeyPress={ props.LOGO_CONCERT_VtwooCOLORoneonKeyPress } onDrag={ props.LOGO_CONCERT_VtwooCOLORoneonDrag } onMouseLeave={ props.LOGO_CONCERT_VtwooCOLORoneonMouseLeave } onMouseUp={ props.LOGO_CONCERT_VtwooCOLORoneonMouseUp } onMouseDown={ props.LOGO_CONCERT_VtwooCOLORoneonMouseDown } onKeyDown={ props.LOGO_CONCERT_VtwooCOLORoneonKeyDown } onChange={ props.LOGO_CONCERT_VtwooCOLORoneonChange } ondelay={ props.LOGO_CONCERT_VtwooCOLORoneondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['logo_concert_vtwoocolortwoo']?.animationClass || {}}>

              <div id="id_threefourtwoo_oneeightthreeone" className={` frame logo_concert_vtwoocolortwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['logo_concert_vtwoocolortwoo']?.type ? transaction['logo_concert_vtwoocolortwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LOGO_CONCERT_VtwooCOLORtwooStyle , transitionDuration: transaction['logo_concert_vtwoocolortwoo']?.duration, transitionTimingFunction: transaction['logo_concert_vtwoocolortwoo']?.timingFunction } } onClick={ props.LOGO_CONCERT_VtwooCOLORtwooonClick } onMouseEnter={ props.LOGO_CONCERT_VtwooCOLORtwooonMouseEnter } onMouseOver={ props.LOGO_CONCERT_VtwooCOLORtwooonMouseOver } onKeyPress={ props.LOGO_CONCERT_VtwooCOLORtwooonKeyPress } onDrag={ props.LOGO_CONCERT_VtwooCOLORtwooonDrag } onMouseLeave={ props.LOGO_CONCERT_VtwooCOLORtwooonMouseLeave } onMouseUp={ props.LOGO_CONCERT_VtwooCOLORtwooonMouseUp } onMouseDown={ props.LOGO_CONCERT_VtwooCOLORtwooonMouseDown } onKeyDown={ props.LOGO_CONCERT_VtwooCOLORtwooonKeyDown } onChange={ props.LOGO_CONCERT_VtwooCOLORtwooonChange } ondelay={ props.LOGO_CONCERT_VtwooCOLORtwooondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_threefourtwoo_oneeightthreetwoo" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_threefourtwoo_oneeightthreethree" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                          <div id="id_threefourtwoo_oneeightthreefour" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_threefourtwoo_oneeightthreefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="126.04385375976562" height="140.1868896484375">
                                <path d="M51.863 32.0969C25.9798 44.4031 8.47525 69.5484 5.96698 98.263C-8.38888 65.7127 3.93902 27.6752 34.572 9.66861C65.2049 -8.33794 104.537 -0.55996 126.044 27.7284C107.685 20.9094 87.5659 20.2168 68.7805 25.5442C95.6244 74.1832 109.02 98.4228 109.02 98.4228C100.107 100.074 92.1555 105.402 87.2457 113.073C82.3359 120.744 80.7882 130.174 82.9763 138.911C66.0054 143.173 56.3993 136.727 54.1045 120.052C54.3713 104.443 63.5505 92.1897 82.0157 83.3996C91.0348 79.0311 84.8975 67.9502 75.7183 73.5439C45.8859 91.4439 36.2797 112.381 46.8465 136.3C38.4678 133.424 30.8362 128.895 24.272 123.195C11.6773 91.2308 22.6176 63.7415 57.4132 40.7805C66.379 34.8138 61.149 27.6752 51.863 32.0969Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_threefourtwoo_oneeightthreesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.331436157226562" height="21.072120666503906">
                                <path d="M9.44054 3.15764C16.6985 -0.944442 24.3834 -1.10427 26.6782 2.99782C28.973 7.0999 25.0238 13.6526 17.926 17.9145C10.668 22.0166 2.98307 22.1764 0.688266 18.0743C-1.6599 13.9722 2.18256 7.25972 9.44054 3.15764Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_threefourtwoo_oneeightthreeseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="54.05384826660156" height="48.16148376464844">
                                <path d="M53.1007 48.1615C54.4882 40.1171 54.3281 31.913 52.8338 23.8154C40.2391 7.56685 20.3864 -1.32988 0 0.161783C11.2072 20.9386 16.8641 31.327 16.8641 31.327C30.9531 30.6344 44.5085 36.9207 53.1007 48.1615Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_threefourtwoo_oneeightthreeeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="19.31903076171875" height="28.17132568359375">
                                <path d="M18.0916 24.5458C18.8921 16.3417 19.319 12.1331 19.319 12.1331C11.7942 1.74466 5.33675 -1.93124 0 0.945551C7.09788 14.2107 11.6341 22.2551 13.2885 25.3982C15.4766 29.5003 17.6646 28.9143 18.0916 24.5458Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_threefourtwoo_oneeightthreenigth" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="100.06663513183594" height="74.47825622558594">
                            <path d="M71.886 0.00147018C87.2559 -0.158351 100.278 12.7339 100.064 28.13C100.278 43.4729 87.2559 56.4717 71.886 56.2586L29.0853 56.2586L29.0853 74.4783L0 74.4783L0 0.00147018L71.886 0.00147018ZM64.628 34.4696C66.3358 34.4696 67.8301 33.8836 69.0575 32.6583C70.3384 31.3797 70.9254 29.8881 70.9254 28.1833C70.9254 26.4253 70.3384 24.9336 69.0575 23.7083C67.8301 22.4297 66.3358 21.8437 64.628 21.8437L29.0853 21.8437L29.0853 34.5229L64.628 34.5229L64.628 34.4696Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="87.30924224853516" height="74.47677612304688">
                            <path d="M29.0853 0L29.0853 50.8765L87.3092 50.8765L87.3092 74.4768L0 74.4768L0 0L29.0853 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourtwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="114.63339233398438" height="74.4767837524414">
                            <path d="M40.0256 0L74.6077 0L114.633 74.4768L81.8658 74.4768L75.9953 61.7444L38.6381 61.7444L32.7677 74.4768L0 74.4768L40.0256 0ZM47.0701 43.578L67.5632 43.578L57.3701 21.5759L47.0701 43.578Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="96.38168334960938" height="74.4767837524414">
                            <path d="M0 23.6003L0 0L96.3817 0L96.3817 23.6003L45.4691 50.8232L96.3817 50.8232L96.3817 74.4768L0 74.4768L0 50.8232L49.0981 23.6003L0 23.6003Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="114.6334228515625" height="74.4767837524414">
                            <path d="M40.0256 0L74.6078 0L114.633 74.4768L81.8658 74.4768L75.9953 61.7444L38.6381 61.7444L32.7677 74.4768L0 74.4768L40.0256 0ZM47.0702 43.578L67.5633 43.578L57.3701 21.5759L47.0702 43.578Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_threefourtwoo_oneeightfourfive" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfoursix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="71.08563995361328" height="54.60565185546875">
                            <path d="M71.0856 31.7512C69.8048 49.3315 54.3816 54.6057 35.5963 54.6057C16.0638 54.5524 -0.0532353 48.9054 0.000132155 29.194L0.000132155 25.4116C-0.0532353 5.75357 16.0638 0.106548 35.5963 0C54.3816 0.0532738 69.8048 5.32738 71.0856 22.8545L50.2723 22.8545C48.3511 17.7402 42.5874 16.5682 35.5963 16.5682C27.3243 16.6747 20.2264 18.1664 20.3332 26.6902L20.3332 27.9688C20.2264 36.4393 27.2709 37.9842 35.5963 38.0908C42.5874 38.0908 48.4045 36.8655 50.2723 31.7512L71.0856 31.7512Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="71.13914489746094" height="54.60565185546875">
                            <path d="M71.139 25.4116L71.139 29.194C71.1924 48.8521 55.1821 54.5524 35.5962 54.6057C15.957 54.5524 -0.0532345 48.8521 0.000133033 29.194L0.000133033 25.4116C-0.0532345 5.7003 15.957 0.0532738 35.5962 0C55.2355 0 71.139 5.75357 71.139 25.4116ZM50.8594 26.6369C50.9661 18.1664 43.9216 16.6214 35.5962 16.5149C27.2709 16.6214 20.2264 18.1664 20.3332 26.6369L20.3332 27.9155C20.2264 36.4393 27.2709 37.9842 35.5962 38.0908C43.8682 37.9842 50.9661 36.4393 50.8594 27.9155L50.8594 26.6369Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfoureight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="71.24560546875" height="52.04851531982422">
                            <path d="M50.9126 0L71.2456 0L71.2456 52.0485L48.351 52.0485L20.333 20.244L20.333 52.0485L0 52.0485L0 0L27.9646 0L50.9126 27.5958L50.9126 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfournigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="71.08563232421875" height="54.60565185546875">
                            <path d="M71.0856 31.7512C69.8048 49.3315 54.3816 54.6057 35.5963 54.6057C16.0638 54.5524 -0.0532353 48.9054 0.000132155 29.194L0.000132155 25.4116C-0.0532353 5.75357 16.0638 0.106548 35.5963 0C54.3816 0.0532738 69.8048 5.32738 71.0856 22.8545L50.2723 22.8545C48.3511 17.7402 42.5874 16.5682 35.5963 16.5682C27.3243 16.6747 20.2264 18.1664 20.3332 26.6902L20.3332 27.9688C20.2264 36.4393 27.2709 37.9842 35.5963 38.0908C42.5874 38.0908 48.4045 36.8655 50.2723 31.7512L71.0856 31.7512Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfivezero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="60.999053955078125" height="52.04851531982422">
                            <path d="M0 0L60.999 0L60.999 13.3185L20.333 13.3185L20.333 19.9777L60.999 19.9777L60.999 32.0176L20.333 32.0176L20.333 38.6768L60.999 38.6768L60.999 52.0485L0 52.0485L0 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfiveone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="69.91351318359375" height="52.04944610595703">
                            <path d="M50.2189 0.000936104C60.9457 -0.105612 70.0715 8.89766 69.9114 19.659C69.9114 23.548 68.8441 27.1173 66.7628 30.3137C64.6815 33.4569 61.9597 35.8542 58.5442 37.4524L69.9114 52.0494L48.2976 52.0494L38.478 39.3703L20.3331 39.3703L20.3331 52.0494L0 52.0494L0 0.000936104L50.2189 0.000936104ZM20.2796 15.2372L20.2796 24.0807L45.0422 24.0807C46.2696 24.0807 47.337 23.6545 48.1909 22.8021C49.0981 21.8965 49.5251 20.8843 49.5251 19.659C49.5251 17.1551 47.5505 15.184 45.0422 15.184L20.2796 15.184L20.2796 15.2372Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfivetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="68.63055419921875" height="52.04850769042969">
                            <path d="M24.1754 52.0485L24.1754 16.5149L0 16.5149L0 0L68.6306 0L68.6306 16.5149L44.4551 16.5149L44.4551 52.0485L24.1754 52.0485Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['logo_concertone']?.animationClass || {}}>

          <div id="id_threefourtwoo_oneeightfivethree" className={` frame logo_concertone ${ props.onClick ? 'cursor' : '' } ${ transaction['logo_concertone']?.type ? transaction['logo_concertone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LOGO_CONCERToneStyle , transitionDuration: transaction['logo_concertone']?.duration, transitionTimingFunction: transaction['logo_concertone']?.timingFunction } } onClick={ props.LOGO_CONCERToneonClick } onMouseEnter={ props.LOGO_CONCERToneonMouseEnter } onMouseOver={ props.LOGO_CONCERToneonMouseOver } onKeyPress={ props.LOGO_CONCERToneonKeyPress } onDrag={ props.LOGO_CONCERToneonDrag } onMouseLeave={ props.LOGO_CONCERToneonMouseLeave } onMouseUp={ props.LOGO_CONCERToneonMouseUp } onMouseDown={ props.LOGO_CONCERToneonMouseDown } onKeyDown={ props.LOGO_CONCERToneonKeyDown } onChange={ props.LOGO_CONCERToneonChange } ondelay={ props.LOGO_CONCERToneondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_threefourtwoo_oneeightfivefour" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_threefourtwoo_oneeightfivefive" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_threefourtwoo_oneeightfivesix" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfiveseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="150.42269897460938" height="167.59771728515625">
                            <path d="M61.8941 38.3728C31.0047 53.0853 10.1145 83.1472 7.12109 117.476C-10.0114 78.5615 4.70088 33.0865 41.2587 11.5591C77.8166 -9.96826 124.756 -0.669449 150.423 33.1502C128.513 24.9978 104.502 24.1698 82.0837 30.5389C114.12 88.6883 130.106 117.667 130.106 117.667C119.47 119.642 109.98 126.011 104.12 135.182C98.2609 144.354 96.4139 155.627 99.0252 166.072C78.7719 171.167 67.3078 163.461 64.5691 143.526C64.8876 124.864 75.8422 110.216 97.8788 99.7067C108.642 94.4841 101.318 81.2365 90.3634 87.924C54.7609 109.324 43.2968 134.354 55.9073 162.951C45.9081 159.512 36.8004 154.098 28.9666 147.284C13.9359 109.069 26.9922 76.205 68.5179 48.7544C79.2177 41.621 72.9761 33.0865 61.8941 38.3728Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfiveeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="32.617767333984375" height="25.1923828125">
                            <path d="M11.2665 3.77506C19.9283 -1.12911 29.0996 -1.32018 31.8382 3.58398C34.5769 8.48815 29.8638 16.3221 21.3931 21.4173C12.7313 26.3215 3.56004 26.5126 0.821387 21.6084C-1.98096 16.7042 2.6047 8.67922 11.2665 3.77506Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightfivenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="64.50869750976562" height="57.57855224609375">
                            <path d="M63.3712 57.5785C65.0271 47.9613 64.836 38.1529 63.0527 28.472C48.022 9.0464 24.3294 -1.58992 0 0.193416C13.3748 25.0327 20.1259 37.4523 20.1259 37.4523C36.94 36.6244 53.1171 44.1398 63.3712 57.5785Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_threefourtwoo_oneeightsixzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="23.055633544921875" height="33.6796875">
                            <path d="M21.5908 29.3453C22.5461 19.537 23.0556 14.5054 23.0556 14.5054C14.0754 2.08579 6.36896 -2.30885 0 1.13043C8.47072 16.9894 13.8843 26.6066 15.8587 30.3644C18.47 35.2685 21.0813 34.5679 21.5908 29.3453Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_threefourtwoo_oneeightsixone" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixtwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="119.42109680175781" height="89.041015625">
                        <path d="M85.7899 0.00175764C104.133 -0.189314 119.673 15.2238 119.418 33.6303C119.673 51.9732 104.133 67.5137 85.7899 67.2589L34.7108 67.2589L34.7108 89.041L0 89.041L0 0.00175764L85.7899 0.00175764ZM77.1281 41.2095C79.1662 41.2095 80.9495 40.5089 82.4143 39.044C83.9429 37.5154 84.6435 35.7321 84.6435 33.694C84.6435 31.5922 83.9429 29.8089 82.4143 28.344C80.9495 26.8154 79.1662 26.1149 77.1281 26.1149L34.7108 26.1149L34.7108 41.2732L77.1281 41.2732L77.1281 41.2095Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="104.19622039794922" height="89.039306640625">
                        <path d="M34.7108 0L34.7108 60.8244L104.196 60.8244L104.196 89.0393L0 89.0393L0 0L34.7108 0Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="136.80526733398438" height="89.039306640625">
                        <path d="M47.7672 0L89.0381 0L136.805 89.0393L97.6999 89.0393L90.694 73.8173L46.1113 73.8173L39.1054 89.0393L0 89.0393L47.7672 0ZM56.1742 52.0988L80.631 52.0988L68.4663 25.7946L56.1742 52.0988Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="115.02340698242188" height="89.039306640625">
                        <path d="M0 28.2149L0 0L115.023 0L115.023 28.2149L54.2635 60.7607L115.023 60.7607L115.023 89.0393L0 89.0393L0 60.7607L58.5944 28.2149L0 28.2149Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixsix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="136.80532836914062" height="89.039306640625">
                        <path d="M47.7672 0L89.0381 0L136.805 89.0393L97.6999 89.0393L90.694 73.8173L46.1113 73.8173L39.1054 89.0393L0 89.0393L47.7672 0ZM56.1743 52.0988L80.6311 52.0988L68.4663 25.7946L56.1743 52.0988Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_threefourtwoo_oneeightsixseven" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.834716796875" height="65.28274536132812">
                        <path d="M84.8347 37.9595C83.3062 58.9774 64.8999 65.2827 42.4811 65.2827C19.1707 65.219 -0.0635319 58.4679 0.000157716 34.9024L0.000157716 30.3804C-0.0635319 6.87857 19.1707 0.127381 42.4811 0C64.8999 0.0636905 83.3062 6.36905 84.8347 27.3232L59.9958 27.3232C57.7029 21.2089 50.8245 19.8077 42.4811 19.8077C32.6092 19.9351 24.1385 21.7185 24.2659 31.9089L24.2659 33.4375C24.1385 43.5643 32.5456 45.4113 42.4811 45.5387C50.8245 45.5387 57.7666 44.0738 59.9958 37.9595L84.8347 37.9595Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsixnigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.89857482910156" height="65.28274536132812">
                        <path d="M84.8984 30.3804L84.8984 34.9024C84.9621 58.4042 65.8552 65.219 42.4811 65.2827C19.0433 65.219 -0.0635308 58.4042 0.000158763 34.9024L0.000158763 30.3804C-0.0635308 6.81488 19.0433 0.0636905 42.4811 0C65.9189 0 84.8984 6.87857 84.8984 30.3804ZM60.6964 31.8452C60.8237 21.7185 52.4167 19.8714 42.4811 19.744C32.5455 19.8714 24.1385 21.7185 24.2659 31.8452L24.2659 33.3738C24.1385 43.5643 32.5455 45.4113 42.4811 45.5387C52.353 45.4113 60.8237 43.5643 60.6964 33.3738L60.6964 31.8452Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsevenzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="85.02561950683594" height="62.225616455078125">
                        <path d="M60.7599 0L85.0256 0L85.0256 62.2256L57.7028 62.2256L24.2658 24.2024L24.2658 62.2256L0 62.2256L0 0L33.3734 0L60.7599 32.9917L60.7599 0Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsevenone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.834716796875" height="65.28274536132812">
                        <path d="M84.8347 37.9595C83.3062 58.9774 64.8999 65.2827 42.4811 65.2827C19.1707 65.219 -0.0635319 58.4679 0.000157716 34.9024L0.000157716 30.3804C-0.0635319 6.87857 19.1707 0.127381 42.4811 0C64.8999 0.0636905 83.3062 6.36905 84.8347 27.3232L59.9958 27.3232C57.7029 21.2089 50.8245 19.8077 42.4811 19.8077C32.6092 19.9351 24.1385 21.7185 24.2659 31.9089L24.2659 33.4375C24.1385 43.5643 32.5456 45.4113 42.4811 45.5387C50.8245 45.5387 57.7666 44.0738 59.9958 37.9595L84.8347 37.9595Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightseventwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="72.79721069335938" height="62.225616455078125">
                        <path d="M0 0L72.7972 0L72.7972 15.9226L24.2657 15.9226L24.2657 23.8839L72.7972 23.8839L72.7972 38.278L24.2657 38.278L24.2657 46.2393L72.7972 46.2393L72.7972 62.2256L0 62.2256L0 0Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightseventhree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="83.43585205078125" height="62.22674560546875">
                        <path d="M59.932 0.00111914C72.7336 -0.126262 83.6244 10.6374 83.4334 23.5029C83.4334 28.1523 82.1596 32.4196 79.6758 36.241C77.1919 39.9987 73.9437 42.8648 69.8675 44.7755L83.4334 62.2267L57.6391 62.2267L45.9203 47.0684L24.2658 47.0684L24.2658 62.2267L0 62.2267L0 0.00111914L59.932 0.00111914ZM24.202 18.2166L24.202 28.7892L53.754 28.7892C55.2189 28.7892 56.4927 28.2797 57.5117 27.2606C58.5945 26.1779 59.104 24.9678 59.104 23.5029C59.104 20.5095 56.7475 18.1529 53.754 18.1529L24.202 18.1529L24.202 18.2166Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_threefourtwoo_oneeightsevenfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="81.90481567382812" height="62.2255859375">
                        <path d="M28.8513 62.2256L28.8513 19.744L0 19.744L0 0L81.9048 0L81.9048 19.744L53.0534 19.744L53.0534 62.2256L28.8513 62.2256Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['logo_concert_darkone']?.animationClass || {}}>

          <div id="id_threefourtwoo_oneeightsevenfive" className={` frame logo_concert_darkone ${ props.onClick ? 'cursor' : '' } ${ transaction['logo_concert_darkone']?.type ? transaction['logo_concert_darkone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LOGO_CONCERT_DARKoneStyle , transitionDuration: transaction['logo_concert_darkone']?.duration, transitionTimingFunction: transaction['logo_concert_darkone']?.timingFunction } } onClick={ props.LOGO_CONCERT_DARKoneonClick } onMouseEnter={ props.LOGO_CONCERT_DARKoneonMouseEnter } onMouseOver={ props.LOGO_CONCERT_DARKoneonMouseOver } onKeyPress={ props.LOGO_CONCERT_DARKoneonKeyPress } onDrag={ props.LOGO_CONCERT_DARKoneonDrag } onMouseLeave={ props.LOGO_CONCERT_DARKoneonMouseLeave } onMouseUp={ props.LOGO_CONCERT_DARKoneonMouseUp } onMouseDown={ props.LOGO_CONCERT_DARKoneonMouseDown } onKeyDown={ props.LOGO_CONCERT_DARKoneonKeyDown } onChange={ props.LOGO_CONCERT_DARKoneonChange } ondelay={ props.LOGO_CONCERT_DARKoneondelay }>

          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

          <div id="id_sixzerotwoo_seventhreenigth" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_sixzerotwoo_sevenfourzero" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_sevenfourone" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_sixzerotwoo_sevenfourtwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="155.09100341796875" height="172.29672241210938">
                        <path d="M63.815 39.4487C31.9669 54.5737 10.4284 85.4784 7.34209 120.77C-10.3221 80.7642 4.84677 34.0142 42.5392 11.8832C80.2316 -10.2477 128.628 -0.688219 155.091 34.0796C132.502 25.6987 107.746 24.8475 84.6312 31.3951C117.661 91.1749 134.143 120.967 134.143 120.967C123.177 122.996 113.393 129.544 107.352 138.973C101.31 148.401 99.4061 159.99 102.098 170.728C81.2165 175.967 69.3966 168.044 66.573 147.55C66.9013 128.365 78.1959 113.306 100.916 102.502C112.014 97.1332 104.462 83.5142 93.1678 90.3892C56.4604 112.389 44.6405 138.121 57.6424 167.52C47.3328 163.984 37.9425 158.419 29.8656 151.413C14.3684 112.127 27.8299 78.3415 70.6443 50.1213C81.6762 42.788 75.2409 34.0142 63.815 39.4487Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_sixzerotwoo_sevenfourthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="33.630035400390625" height="25.898712158203125">
                        <path d="M11.6161 3.8809C20.5467 -1.16077 30.0027 -1.3572 32.8263 3.68447C35.65 8.72614 30.7907 16.7797 22.0571 22.0178C13.1265 27.0595 3.67052 27.2559 0.846878 22.2142C-2.04243 17.1726 2.68554 8.92257 11.6161 3.8809Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_sixzerotwoo_sevenfourfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="66.51071166992188" height="59.192901611328125">
                        <path d="M65.3379 59.1929C67.0452 49.306 66.8482 39.2227 65.0095 29.2703C49.5123 9.30003 25.0845 -1.63449 0 0.198839C13.7899 25.7346 20.7505 38.5024 20.7505 38.5024C38.0864 37.6512 54.7656 45.3774 65.3379 59.1929Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_sixzerotwoo_sevenfourfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="23.771148681640625" height="34.62396240234375">
                        <path d="M22.2608 30.1681C23.2458 20.0847 23.7712 14.9121 23.7712 14.9121C14.5122 2.14427 6.56662 -2.37359 0 1.16213C8.7336 17.4657 14.3152 27.3526 16.3509 31.2157C19.0432 36.2574 21.7355 35.5371 22.2608 30.1681Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_sixzerotwoo_sevenfoursix" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="123.12728118896484" height="91.53753662109375">
                    <path d="M88.4524 0.00180692C107.364 -0.194622 123.387 15.6506 123.124 34.5732C123.387 53.4304 107.364 69.4066 88.4524 69.1447L35.7881 69.1447L35.7881 91.5375L0 91.5375L0 0.00180692L88.4524 0.00180692ZM79.5218 42.3649C81.6231 42.3649 83.4617 41.6447 84.972 40.1387C86.548 38.5673 87.2704 36.734 87.2704 34.6387C87.2704 32.478 86.548 30.6447 84.972 29.1387C83.4617 27.5673 81.6231 26.847 79.5218 26.847L35.7881 26.847L35.7881 42.4304L79.5218 42.4304L79.5218 42.3649Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfoureight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="107.42988586425781" height="91.53570556640625">
                    <path d="M35.7881 0L35.7881 62.5298L107.43 62.5298L107.43 91.5357L0 91.5357L0 0L35.7881 0Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfournigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="141.0509490966797" height="91.53573608398438">
                    <path d="M49.2496 0L91.8013 0L141.051 91.5357L100.732 91.5357L93.5087 75.8869L47.5423 75.8869L40.319 91.5357L0 91.5357L49.2496 0ZM57.9176 53.5595L83.1334 53.5595L70.5911 26.5179L57.9176 53.5595Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivezero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="118.59309387207031" height="91.53573608398438">
                    <path d="M0 29.006L0 0L118.593 0L118.593 29.006L55.9476 62.4643L118.593 62.4643L118.593 91.5357L0 91.5357L0 62.4643L60.4129 29.006L0 29.006Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfiveone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="141.05096435546875" height="91.53573608398438">
                    <path d="M49.2496 0L91.8013 0L141.051 91.5357L100.732 91.5357L93.5087 75.8869L47.5423 75.8869L40.319 91.5357L0 91.5357L49.2496 0ZM57.9176 53.5595L83.1334 53.5595L70.5911 26.5179L57.9176 53.5595Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_sixzerotwoo_sevenfivetwoo" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="87.46751403808594" height="67.11309814453125">
                    <path d="M87.4675 39.0238C85.8915 60.631 66.914 67.1131 43.7995 67.1131C19.7657 67.0476 -0.0655036 60.1071 0.000162611 35.881L0.000162611 31.2321C-0.0655036 7.07143 19.7657 0.130952 43.7995 0C66.914 0.0654762 85.8915 6.54762 87.4675 28.0893L61.8577 28.0893C59.4937 21.8036 52.4018 20.3631 43.7995 20.3631C33.6213 20.494 24.8876 22.3274 25.019 32.8036L25.019 34.375C24.8876 44.7857 33.5556 46.6845 43.7995 46.8155C52.4018 46.8155 59.5594 45.3095 61.8577 39.0238L87.4675 39.0238Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="87.53336334228516" height="67.11309814453125">
                    <path d="M87.5332 31.2321L87.5332 35.881C87.5989 60.0417 67.899 67.0476 43.7995 67.1131C19.6343 67.0476 -0.0655025 60.0417 0.00016369 35.881L0.00016369 31.2321C-0.0655025 7.00595 19.6343 0.0654762 43.7995 0C67.9647 0 87.5332 7.07143 87.5332 31.2321ZM62.58 32.7381C62.7114 22.3274 54.0434 20.4286 43.7995 20.2976C33.5556 20.4286 24.8876 22.3274 25.019 32.7381L25.019 34.3095C24.8876 44.7857 33.5556 46.6845 43.7995 46.8155C53.9778 46.6845 62.7114 44.7857 62.58 34.3095L62.58 32.7381Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="87.66434478759766" height="63.97023010253906">
                    <path d="M62.6455 0L87.6643 0L87.6643 63.9702L59.4936 63.9702L25.0188 24.881L25.0188 63.9702L0 63.9702L0 0L34.4091 0L62.6455 33.9167L62.6455 0Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="87.46751403808594" height="67.11309814453125">
                    <path d="M87.4675 39.0238C85.8915 60.631 66.914 67.1131 43.7995 67.1131C19.7657 67.0476 -0.0655036 60.1071 0.000162611 35.881L0.000162611 31.2321C-0.0655036 7.07143 19.7657 0.130952 43.7995 0C66.914 0.0654762 85.8915 6.54762 87.4675 28.0893L61.8577 28.0893C59.4937 21.8036 52.4018 20.3631 43.7995 20.3631C33.6213 20.494 24.8877 22.3274 25.019 32.8036L25.019 34.375C24.8877 44.7857 33.5556 46.6845 43.7995 46.8155C52.4018 46.8155 59.5594 45.3095 61.8577 39.0238L87.4675 39.0238Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfiveseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="75.05644226074219" height="63.97023010253906">
                    <path d="M0 0L75.0564 0L75.0564 16.369L25.0188 16.369L25.0188 24.5536L75.0564 24.5536L75.0564 39.3512L25.0188 39.3512L25.0188 47.5357L75.0564 47.5357L75.0564 63.9702L0 63.9702L0 0Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfiveeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="86.02523803710938" height="63.97138977050781">
                    <path d="M61.7919 0.00115052C74.9908 -0.129802 86.2197 10.9357 86.0227 24.1619C86.0227 28.9416 84.7094 33.3285 82.1484 37.2571C79.5875 41.1202 76.2385 44.0666 72.0358 46.0309L86.0227 63.9714L59.4279 63.9714L47.3454 48.3881L25.0189 48.3881L25.0189 63.9714L0 63.9714L0 0.00115052L61.7919 0.00115052ZM24.9531 18.7273L24.9531 29.5964L55.4223 29.5964C56.9326 29.5964 58.2459 29.0726 59.2966 28.025C60.4129 26.9119 60.9382 25.6678 60.9382 24.1619C60.9382 21.0845 58.5086 18.6619 55.4223 18.6619L24.9531 18.6619L24.9531 18.7273Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_sixzerotwoo_sevenfivenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.44668579101562" height="63.97023010253906">
                    <path d="M29.7467 63.9702L29.7467 20.2976L0 20.2976L0 0L84.4467 0L84.4467 20.2976L54.6999 20.2976L54.6999 63.9702L29.7467 63.9702Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Logo.propTypes = {
    style: PropTypes.any,
Rectangletwooeight0: PropTypes.any,
LogoonClick: PropTypes.any,
LogoonMouseEnter: PropTypes.any,
LogoonMouseOver: PropTypes.any,
LogoonKeyPress: PropTypes.any,
LogoonDrag: PropTypes.any,
LogoonMouseLeave: PropTypes.any,
LogoonMouseUp: PropTypes.any,
LogoonMouseDown: PropTypes.any,
LogoonKeyDown: PropTypes.any,
LogoonChange: PropTypes.any,
Logoondelay: PropTypes.any,
RectangletwooeightonClick: PropTypes.any,
RectangletwooeightonMouseEnter: PropTypes.any,
RectangletwooeightonMouseOver: PropTypes.any,
RectangletwooeightonKeyPress: PropTypes.any,
RectangletwooeightonDrag: PropTypes.any,
RectangletwooeightonMouseLeave: PropTypes.any,
RectangletwooeightonMouseUp: PropTypes.any,
RectangletwooeightonMouseDown: PropTypes.any,
RectangletwooeightonKeyDown: PropTypes.any,
RectangletwooeightonChange: PropTypes.any,
Rectangletwooeightondelay: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonClick: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonMouseEnter: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonMouseOver: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonKeyPress: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonDrag: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonMouseLeave: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonMouseUp: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonMouseDown: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonKeyDown: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneonChange: PropTypes.any,
LOGO_CONCERT_VtwooCOLORoneondelay: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonClick: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonMouseEnter: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonMouseOver: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonKeyPress: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonDrag: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonMouseLeave: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonMouseUp: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonMouseDown: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonKeyDown: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooonChange: PropTypes.any,
LOGO_CONCERT_VtwooCOLORtwooondelay: PropTypes.any,
GrouponClick: PropTypes.any,
GrouponMouseEnter: PropTypes.any,
GrouponMouseOver: PropTypes.any,
GrouponKeyPress: PropTypes.any,
GrouponDrag: PropTypes.any,
GrouponMouseLeave: PropTypes.any,
GrouponMouseUp: PropTypes.any,
GrouponMouseDown: PropTypes.any,
GrouponKeyDown: PropTypes.any,
GrouponChange: PropTypes.any,
Groupondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
LOGO_CONCERToneonClick: PropTypes.any,
LOGO_CONCERToneonMouseEnter: PropTypes.any,
LOGO_CONCERToneonMouseOver: PropTypes.any,
LOGO_CONCERToneonKeyPress: PropTypes.any,
LOGO_CONCERToneonDrag: PropTypes.any,
LOGO_CONCERToneonMouseLeave: PropTypes.any,
LOGO_CONCERToneonMouseUp: PropTypes.any,
LOGO_CONCERToneonMouseDown: PropTypes.any,
LOGO_CONCERToneonKeyDown: PropTypes.any,
LOGO_CONCERToneonChange: PropTypes.any,
LOGO_CONCERToneondelay: PropTypes.any,
LOGO_CONCERT_DARKoneonClick: PropTypes.any,
LOGO_CONCERT_DARKoneonMouseEnter: PropTypes.any,
LOGO_CONCERT_DARKoneonMouseOver: PropTypes.any,
LOGO_CONCERT_DARKoneonKeyPress: PropTypes.any,
LOGO_CONCERT_DARKoneonDrag: PropTypes.any,
LOGO_CONCERT_DARKoneonMouseLeave: PropTypes.any,
LOGO_CONCERT_DARKoneonMouseUp: PropTypes.any,
LOGO_CONCERT_DARKoneonMouseDown: PropTypes.any,
LOGO_CONCERT_DARKoneonKeyDown: PropTypes.any,
LOGO_CONCERT_DARKoneonChange: PropTypes.any,
LOGO_CONCERT_DARKoneondelay: PropTypes.any
}
export default Logo;