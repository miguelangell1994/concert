import CorebooksAnimation from "Components/CorebooksAnimation";
import React from "react";
import "./Overlay.css";

const getPosition = (position) => {
  const styled = { position: "fixed" };

  switch (position.toLowerCase()) {
    case "center":
      styled["top"] = `50%`;
      styled["left"] = `50%`;
      styled["transform"] = `translate(-50%, -50%)`;
      break;
    case "top_center":
      styled["top"] = `0px`;
      styled["left"] = `50%`;
      styled["transform"] = `translateX(-50%)`;

      break;
    case "top_right":
      styled["right"] = `0px`;
      styled["top"] = `0px`;
      break;
    case "top_left":
      styled["top"] = `0px`;
      styled["left"] = `0px`;
      break;
    case "bottom_center":
      styled["bottom"] = `0px`;
      styled["left"] = `50%`;
      styled["transform"] = `translateX(-50%)`;
      break;
    case "bottom_right":
      styled["right"] = `0px`;
      styled["bottom"] = `0px`;
      break;
    case "bottom_left":
      styled["left"] = `0px`;
      styled["bottom"] = `0px`;
      break;
  }

  return styled;
};

const getProps = (item)=>({
      name: item.destNodeName,
      type: item.navigation,
      width: item.width,
      height: item.height,
      transition: item.transition ? item.transition.type : null,
      direction: item.transition ? item.transition.direction: null,
      position: item.prototyping ? item.prototyping.overlayPositionType : false,
      backGroundInteraction:  item.prototyping ? item.prototyping.overlayBackgroundInteraction : false,
      overlayBackground: item.prototyping,
  })
/**
 * 
 * @param  Components: List<{Component: React.Element, props: Object, selectedName: string}>
 * @param  selectedName: string
 * @param  onClickBack: event
 * @returns 
 */
const Overlay = ({children, Components, selectedName, onClickBack}) => {

  const [_isOpen, setIsOpen] = React.useState(false);
  const [indexSelected, setIndexSelected] = React.useState(-1);
  const [beforeProps, setBeforeProps] = React.useState(false);
  const [active, setActive] = React.useState(false);

  React.useEffect(() => {
    setIsOpen(false);
    if (selectedName) {
      const prevIndex = indexSelected;
      const newIndex = Components.findIndex((component) => component.props.name === selectedName.NameComponent);
      if(prevIndex < 0)
        setCurrentProps(selectedName);
      else 
        setCurrentProps(selectedName);
  
      setIndexSelected(newIndex);
      setTimeout(() => { setIsOpen(true) }, 100);
    }else{
      setIndexSelected(-1);
    }
    
  }, [selectedName]);

  React.useEffect(() => {
    console.log(_isOpen);
    if(_isOpen)
      setTimeout(() => { setActive(true) }, 10);
    else
      setTimeout(() => { setActive(false) }, 10);
  }, [_isOpen]);

  
  const setCurrentProps = (item) => {
   
    setBeforeProps(getProps(item));
  }
  const getComponent = (Component) => {
    return <Component onClick={(e)=>{e.stopPropagation()}}  />;
  }

  const proccesProps = (props) => {
    const position = props.position || beforeProps.position
    return {
      ...props,
      position : position || "center"
    }
  }

 
  if (indexSelected < 0) return null;

  const currentProps = getProps(selectedName)
  return (
    <div
      className={`overlay`}
      onClick={onClickBack}
    >
      <div
        style={{ width: "100vw", height: "100vh", position: "relative" }}
      >
        {
          (_isOpen) &&
          <CorebooksAnimation
            animationType={currentProps.transition}
            direction={currentProps.direction}
            active={active}
            easing="ease-in-out"
            duration="1s"
          >
           
            <div>
              <div style={ getPosition(currentProps.position)} onClick={(e)=>{ e.stopPropagation(); }}  >
                {getComponent(Components[indexSelected].Component)}
              </div>
              
            </div>
             
           
          </CorebooksAnimation>
        }
      </div>
    </div>
  );
};

export default Overlay;