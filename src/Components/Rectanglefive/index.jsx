import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectanglefive.css'





const Rectanglefive = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglefive']?.animationClass || {}}>

    <div id="id_onenigth_twoothreefourtwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } rectanglefive ${ props.cssClass } ${ transaction['rectanglefive']?.type ? transaction['rectanglefive']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['rectanglefive']?.duration, transitionTimingFunction: transaction['rectanglefive']?.timingFunction }, ...props.style }} onClick={ props.RectanglefiveonClick } onMouseEnter={ props.RectanglefiveonMouseEnter } onMouseOver={ props.RectanglefiveonMouseOver } onKeyPress={ props.RectanglefiveonKeyPress } onDrag={ props.RectanglefiveonDrag } onMouseLeave={ props.RectanglefiveonMouseLeave } onMouseUp={ props.RectanglefiveonMouseUp } onMouseDown={ props.RectanglefiveonMouseDown } onKeyDown={ props.RectanglefiveonKeyDown } onChange={ props.RectanglefiveonChange } ondelay={ props.Rectanglefiveondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Rectanglefive.propTypes = {
    style: PropTypes.any,
RectanglefiveonClick: PropTypes.any,
RectanglefiveonMouseEnter: PropTypes.any,
RectanglefiveonMouseOver: PropTypes.any,
RectanglefiveonKeyPress: PropTypes.any,
RectanglefiveonDrag: PropTypes.any,
RectanglefiveonMouseLeave: PropTypes.any,
RectanglefiveonMouseUp: PropTypes.any,
RectanglefiveonMouseDown: PropTypes.any,
RectanglefiveonKeyDown: PropTypes.any,
RectanglefiveonChange: PropTypes.any,
Rectanglefiveondelay: PropTypes.any
}
export default Rectanglefive;