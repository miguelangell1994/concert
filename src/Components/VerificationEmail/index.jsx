import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './VerificationEmail.css'





const VerificationEmail = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['verificationemail']?.animationClass || {}}>

    <div id="id_sixzerotwoo_oneonetwoofive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } verificationemail ${ props.cssClass } ${ transaction['verificationemail']?.type ? transaction['verificationemail']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['verificationemail']?.duration, transitionTimingFunction: transaction['verificationemail']?.timingFunction }, ...props.style }} onClick={ props.VerificationEmailonClick } onMouseEnter={ props.VerificationEmailonMouseEnter } onMouseOver={ props.VerificationEmailonMouseOver } onKeyPress={ props.VerificationEmailonKeyPress } onDrag={ props.VerificationEmailonDrag } onMouseLeave={ props.VerificationEmailonMouseLeave } onMouseUp={ props.VerificationEmailonMouseUp } onMouseDown={ props.VerificationEmailonMouseDown } onKeyDown={ props.VerificationEmailonKeyDown } onChange={ props.VerificationEmailonChange } ondelay={ props.VerificationEmailondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['box']?.animationClass || {}}>

          <div id="id_sixzerotwoo_oneonetwoosix" className={` frame box ${ props.onClick ? 'cursor' : '' } ${ transaction['box']?.type ? transaction['box']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.BoxStyle , transitionDuration: transaction['box']?.duration, transitionTimingFunction: transaction['box']?.timingFunction } } onClick={ props.BoxonClick } onMouseEnter={ props.BoxonMouseEnter } onMouseOver={ props.BoxonMouseOver } onKeyPress={ props.BoxonKeyPress } onDrag={ props.BoxonDrag } onMouseLeave={ props.BoxonMouseLeave } onMouseUp={ props.BoxonMouseUp } onMouseDown={ props.BoxonMouseDown } onKeyDown={ props.BoxonKeyDown } onChange={ props.BoxonChange } ondelay={ props.Boxondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainheader']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonetwooseven" className={` frame webmainheader ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainheader']?.type ? transaction['webmainheader']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.WebMainHeaderStyle , transitionDuration: transaction['webmainheader']?.duration, transitionTimingFunction: transaction['webmainheader']?.timingFunction } } onClick={ props.WebMainHeaderonClick } onMouseEnter={ props.WebMainHeaderonMouseEnter } onMouseOver={ props.WebMainHeaderonMouseOver } onKeyPress={ props.WebMainHeaderonKeyPress } onDrag={ props.WebMainHeaderonDrag } onMouseLeave={ props.WebMainHeaderonMouseLeave } onMouseUp={ props.WebMainHeaderonMouseUp } onMouseDown={ props.WebMainHeaderonMouseDown } onKeyDown={ props.WebMainHeaderonKeyDown } onChange={ props.WebMainHeaderonChange } ondelay={ props.WebMainHeaderondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonetwooeight" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonetwoonigth" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                          <div id="id_sixzerotwoo_oneonethreezero" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonethreeone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="39.62890625" height="44.0753173828125">
                                <path d="M16.3059 10.0914C8.16815 13.9605 2.66465 21.8663 1.87604 30.8943C-2.6375 20.6603 1.23844 8.70118 10.8696 3.03985C20.5007 -2.62148 32.8668 -0.176054 39.6287 8.71793C33.8567 6.574 27.5311 6.35625 21.6249 8.0312C30.0647 23.3235 34.2762 30.9445 34.2762 30.9445C31.4741 31.4638 28.9741 33.1387 27.4304 35.5506C25.8867 37.9626 25.4001 40.9272 26.0881 43.6741C20.7524 45.0141 17.7322 42.9874 17.0107 37.7448C17.0946 32.8372 19.9805 28.9848 25.7861 26.2212C28.6217 24.8477 26.6921 21.3638 23.8061 23.1225C14.4267 28.7503 11.4065 35.3329 14.7287 42.8534C12.0944 41.9489 9.69504 40.5252 7.63123 38.733C3.67139 28.6833 7.11108 20.0406 18.051 12.8216C20.8698 10.9456 19.2255 8.70118 16.3059 10.0914Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonethreetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="8.59326171875" height="6.625152587890625">
                                <path d="M2.96814 0.992775C5.25008 -0.296936 7.66625 -0.347185 8.38775 0.942526C9.10924 2.23224 7.8676 4.29242 5.636 5.63238C3.35406 6.9221 0.937889 6.97235 0.216394 5.68264C-0.521881 4.39292 0.686206 2.28249 2.96814 0.992775Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonethreethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16.99462890625" height="15.142181396484375">
                                <path d="M16.6951 15.1422C17.1313 12.613 17.081 10.0336 16.6112 7.48765C12.6513 2.37905 6.40956 -0.418121 0 0.0508652C3.52358 6.58317 5.30215 9.84933 5.30215 9.84933C9.73179 9.63158 13.9937 11.608 16.6951 15.1422Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonethreefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="6.07421875" height="8.857177734375">
                                <path d="M5.68807 7.71732C5.93975 5.13789 6.07398 3.81468 6.07398 3.81468C3.70815 0.548528 1.6779 -0.607188 0 0.297285C2.2316 4.46791 3.65781 6.99709 4.17796 7.98531C4.8659 9.27502 5.55384 9.09077 5.68807 7.71732Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonethreefive" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonethreesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="31.46142578125" height="23.416259765625">
                            <path d="M22.6013 0.00046223C27.4336 -0.0497863 31.5277 4.00359 31.4605 8.8442C31.5277 13.6681 27.4336 17.7549 22.6013 17.6879L9.14453 17.6879L9.14453 23.4163L0 23.4163L0 0.00046223L22.6013 0.00046223ZM20.3193 10.8374C20.8562 10.8374 21.3261 10.6531 21.712 10.2679C22.1147 9.86592 22.2992 9.39693 22.2992 8.86095C22.2992 8.30822 22.1147 7.83923 21.712 7.45399C21.3261 7.052 20.8562 6.86776 20.3193 6.86776L9.14453 6.86776L9.14453 10.8541L20.3193 10.8541L20.3193 10.8374Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonethreeseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="27.4501953125" height="23.415802001953125">
                            <path d="M9.14453 0L9.14453 15.9958L27.4504 15.9958L27.4504 23.4158L0 23.4158L0 0L9.14453 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonethreeeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="36.041015625" height="23.415802001953125">
                            <path d="M12.5842 0L23.457 0L36.0412 23.4158L25.7389 23.4158L23.8932 19.4127L12.148 19.4127L10.3023 23.4158L0 23.4158L12.5842 0ZM14.799 13.7011L21.2422 13.7011L18.0374 6.78355L14.799 13.7011Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonethreenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="30.302734375" height="23.415802001953125">
                            <path d="M0 7.42003L0 0L30.3028 0L30.3028 7.42003L14.2957 15.979L30.3028 15.979L30.3028 23.4158L0 23.4158L0 15.979L15.4366 7.42003L0 7.42003Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="36.041015625" height="23.415802001953125">
                            <path d="M12.5842 0L23.457 0L36.0412 23.4158L25.7389 23.4158L23.8932 19.4127L12.148 19.4127L10.3023 23.4158L0 23.4158L12.5842 0ZM14.7991 13.7011L21.2422 13.7011L18.0374 6.78355L14.7991 13.7011Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonefourone" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourtwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22.349609375" height="17.168243408203125">
                            <path d="M22.3496 9.9827C21.9469 15.51 17.0978 17.1682 11.1916 17.1682C5.05051 17.1515 -0.0167374 15.376 4.15501e-05 9.17873L4.15501e-05 7.98951C-0.0167374 1.80895 5.05051 0.033499 11.1916 0C17.0978 0.0167495 21.9469 1.67495 22.3496 7.18554L15.8058 7.18554C15.2018 5.57759 13.3897 5.2091 11.1916 5.2091C8.59087 5.24259 6.35927 5.71158 6.39283 8.3915L6.39283 8.79349C6.35927 11.4567 8.57409 11.9424 11.1916 11.9759C13.3897 11.9759 15.2186 11.5907 15.8058 9.9827L22.3496 9.9827Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22.3662109375" height="17.168243408203125">
                            <path d="M22.3664 7.98951L22.3664 9.17873C22.3832 15.3593 17.3495 17.1515 11.1916 17.1682C5.01695 17.1515 -0.0167371 15.3593 4.1826e-05 9.17873L4.1826e-05 7.98951C-0.0167371 1.7922 5.01695 0.0167495 11.1916 0C17.3663 0 22.3664 1.80895 22.3664 7.98951ZM15.9904 8.37475C16.024 5.71158 13.8091 5.22585 11.1916 5.19235C8.57409 5.22585 6.35927 5.71158 6.39283 8.37475L6.39283 8.77674C6.35927 11.4567 8.57409 11.9424 11.1916 11.9759C13.7923 11.9424 16.024 11.4567 15.9904 8.77674L15.9904 8.37475Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22.39990234375" height="16.3642578125">
                            <path d="M16.0071 0L22.3999 0L22.3999 16.3643L15.2017 16.3643L6.39279 6.36481L6.39279 16.3643L0 16.3643L0 0L8.79218 0L16.0071 8.67624L16.0071 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22.349609375" height="17.168243408203125">
                            <path d="M22.3496 9.9827C21.9469 15.51 17.0978 17.1682 11.1916 17.1682C5.05051 17.1515 -0.0167374 15.376 4.15501e-05 9.17873L4.15501e-05 7.98951C-0.0167374 1.80895 5.05051 0.033499 11.1916 0C17.0978 0.0167495 21.9469 1.67495 22.3496 7.18554L15.8058 7.18554C15.2018 5.57759 13.3897 5.2091 11.1916 5.2091C8.59087 5.24259 6.35927 5.71158 6.39283 8.3915L6.39283 8.79349C6.35927 11.4567 8.57409 11.9424 11.1916 11.9759C13.3897 11.9759 15.2186 11.5907 15.8058 9.9827L22.3496 9.9827Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefoursix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="19.17822265625" height="16.3642578125">
                            <path d="M0 0L19.1783 0L19.1783 4.18738L6.39278 4.18738L6.39278 6.28106L19.1783 6.28106L19.1783 10.0665L6.39278 10.0665L6.39278 12.1601L19.1783 12.1601L19.1783 16.3643L0 16.3643L0 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.98095703125" height="16.36456298828125">
                            <path d="M15.789 0.000294315C19.1616 -0.0332047 22.0308 2.79746 21.9804 6.18086C21.9804 7.40358 21.6449 8.52579 20.9905 9.53076C20.3361 10.519 19.4804 11.2727 18.4065 11.7752L21.9804 16.3646L15.185 16.3646L12.0976 12.3782L6.3928 12.3782L6.3928 16.3646L0 16.3646L0 0.000294315L15.789 0.000294315ZM6.376 4.79065L6.376 7.57107L14.1614 7.57107C14.5474 7.57107 14.8829 7.43708 15.1514 7.16908C15.4367 6.88434 15.5709 6.5661 15.5709 6.18086C15.5709 5.39364 14.9501 4.7739 14.1614 4.7739L6.376 4.7739L6.376 4.79065Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonefoureight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.57763671875" height="16.3642578125">
                            <path d="M7.60086 16.3643L7.60086 5.19235L0 5.19235L0 0L21.5777 0L21.5777 5.19235L13.9769 5.19235L13.9769 16.3643L7.60086 16.3643Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainline']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefournigth" className={` instance webmainline ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainline']?.type ? transaction['webmainline']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebMainLineStyle , transitionDuration: transaction['webmainline']?.duration, transitionTimingFunction: transaction['webmainline']?.timingFunction }} onClick={ props.WebMainLineonClick } onMouseEnter={ props.WebMainLineonMouseEnter } onMouseOver={ props.WebMainLineonMouseOver } onKeyPress={ props.WebMainLineonKeyPress } onDrag={ props.WebMainLineonDrag } onMouseLeave={ props.WebMainLineonMouseLeave } onMouseUp={ props.WebMainLineonMouseUp } onMouseDown={ props.WebMainLineonMouseDown } onKeyDown={ props.WebMainLineonKeyDown } onChange={ props.WebMainLineonChange } ondelay={ props.WebMainLineondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonefournigth_five_eightfivethree" className={` instance line ${ props.onClick ? 'cursor' : '' } ${ transaction['line']?.type ? transaction['line']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineStyle , transitionDuration: transaction['line']?.duration, transitionTimingFunction: transaction['line']?.timingFunction }} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>

                      <img id="id_sixzerotwoo_oneonefournigth_five_eightfivethree_onetwoozerotwoo_onesixsixone" className={` line line ${ props.onClick ? 'cursor' : '' } ${ transaction['line']?.type ? transaction['line']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineStyle , transitionDuration: transaction['line']?.duration, transitionTimingFunction: transaction['line']?.timingFunction }} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay } src={props.Line0 || "" } />

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webtexttitlesubtitle']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefivezero" className={` instance webtexttitlesubtitle ${ props.onClick ? 'cursor' : '' } ${ transaction['webtexttitlesubtitle']?.type ? transaction['webtexttitlesubtitle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebTextTitleSubtitleStyle , transitionDuration: transaction['webtexttitlesubtitle']?.duration, transitionTimingFunction: transaction['webtexttitlesubtitle']?.timingFunction }} onClick={ props.WebTextTitleSubtitleonClick } onMouseEnter={ props.WebTextTitleSubtitleonMouseEnter } onMouseOver={ props.WebTextTitleSubtitleonMouseOver } onKeyPress={ props.WebTextTitleSubtitleonKeyPress } onDrag={ props.WebTextTitleSubtitleonDrag } onMouseLeave={ props.WebTextTitleSubtitleonMouseLeave } onMouseUp={ props.WebTextTitleSubtitleonMouseUp } onMouseDown={ props.WebTextTitleSubtitleonMouseDown } onKeyDown={ props.WebTextTitleSubtitleonKeyDown } onChange={ props.WebTextTitleSubtitleonChange } ondelay={ props.WebTextTitleSubtitleondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <span id="id_sixzerotwoo_oneonefivezero_four_fivezeronigth"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text0 || `Confirm your email and start chatting`}</span>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <span id="id_sixzerotwoo_oneonefivezero_four_fiveonezero"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text1 || `Subtitle of letter to explain more!`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webtextl']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefivefour" className={` instance webtextl ${ props.onClick ? 'cursor' : '' } ${ transaction['webtextl']?.type ? transaction['webtextl']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebTextLStyle , transitionDuration: transaction['webtextl']?.duration, transitionTimingFunction: transaction['webtextl']?.timingFunction }} onClick={ props.WebTextLonClick } onMouseEnter={ props.WebTextLonMouseEnter } onMouseOver={ props.WebTextLonMouseOver } onKeyPress={ props.WebTextLonKeyPress } onDrag={ props.WebTextLonDrag } onMouseLeave={ props.WebTextLonMouseLeave } onMouseUp={ props.WebTextLonMouseUp } onMouseDown={ props.WebTextLonMouseDown } onKeyDown={ props.WebTextLonKeyDown } onChange={ props.WebTextLonChange } ondelay={ props.WebTextLondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <span id="id_sixzerotwoo_oneonefivefour_three_four"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text2 || `Estás a un paso de unirte a nuestra comunidad. Solo necesitamos verificar tu dirección de correo electrónico para que puedas sumergirte en la experiencia musical. Haz clic en el enlace de abajo para confirmar tu cuenta.`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainbutton_one']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefivefive" className={` frame webmainbutton_one ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainbutton_one']?.type ? transaction['webmainbutton_one']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.WebMainButton_OneStyle , transitionDuration: transaction['webmainbutton_one']?.duration, transitionTimingFunction: transaction['webmainbutton_one']?.timingFunction } } onClick={ props.WebMainButton_OneonClick } onMouseEnter={ props.WebMainButton_OneonMouseEnter } onMouseOver={ props.WebMainButton_OneonMouseOver } onKeyPress={ props.WebMainButton_OneonKeyPress } onDrag={ props.WebMainButton_OneonDrag } onMouseLeave={ props.WebMainButton_OneonMouseLeave } onMouseUp={ props.WebMainButton_OneonMouseUp } onMouseDown={ props.WebMainButton_OneonMouseDown } onKeyDown={ props.WebMainButton_OneonKeyDown } onChange={ props.WebMainButton_OneonChange } ondelay={ props.WebMainButton_Oneondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['button']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonefivesix" className={` frame button ${ props.onClick ? 'cursor' : '' } ${ transaction['button']?.type ? transaction['button']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ButtonStyle , transitionDuration: transaction['button']?.duration, transitionTimingFunction: transaction['button']?.timingFunction } } onClick={ props.ButtononClick } onMouseEnter={ props.ButtononMouseEnter } onMouseOver={ props.ButtononMouseOver } onKeyPress={ props.ButtononKeyPress } onDrag={ props.ButtononDrag } onMouseLeave={ props.ButtononMouseLeave } onMouseUp={ props.ButtononMouseUp } onMouseDown={ props.ButtononMouseDown } onKeyDown={ props.ButtononKeyDown } onChange={ props.ButtononChange } ondelay={ props.Buttonondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['verificatuemail']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonefiveseven"  className={` text verificatuemail    ${ props.onClick ? 'cursor' : ''}  ${ transaction['verificatuemail']?.type ? transaction['verificatuemail']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.VerificatuEmailStyle , transitionDuration: transaction['verificatuemail']?.duration, transitionTimingFunction: transaction['verificatuemail']?.timingFunction }} onClick={ props.VerificatuEmailonClick } onMouseEnter={ props.VerificatuEmailonMouseEnter } onMouseOver={ props.VerificatuEmailonMouseOver } onKeyPress={ props.VerificatuEmailonKeyPress } onDrag={ props.VerificatuEmailonDrag } onMouseLeave={ props.VerificatuEmailonMouseLeave } onMouseUp={ props.VerificatuEmailonMouseUp } onMouseDown={ props.VerificatuEmailonMouseDown } onKeyDown={ props.VerificatuEmailonKeyDown } onChange={ props.VerificatuEmailonChange } ondelay={ props.VerificatuEmailondelay } >{props.VerificatuEmail0 || `Verifica tu Email`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webtextm']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefiveeight" className={` instance webtextm ${ props.onClick ? 'cursor' : '' } ${ transaction['webtextm']?.type ? transaction['webtextm']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebTextMStyle , transitionDuration: transaction['webtextm']?.duration, transitionTimingFunction: transaction['webtextm']?.timingFunction }} onClick={ props.WebTextMonClick } onMouseEnter={ props.WebTextMonMouseEnter } onMouseOver={ props.WebTextMonMouseOver } onKeyPress={ props.WebTextMonKeyPress } onDrag={ props.WebTextMonDrag } onMouseLeave={ props.WebTextMonMouseLeave } onMouseUp={ props.WebTextMonMouseUp } onMouseDown={ props.WebTextMonMouseDown } onKeyDown={ props.WebTextMonKeyDown } onChange={ props.WebTextMonChange } ondelay={ props.WebTextMondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <span id="id_sixzerotwoo_oneonefiveeight_seven_onenigthsevenfour"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text3 || `Si tienes alguna pregunta acerca de tu cuenta,\npor favor contactanos a suport@concertplaza.com`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainline']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonefivenigth" className={` instance webmainline ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainline']?.type ? transaction['webmainline']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebMainLineStyle , transitionDuration: transaction['webmainline']?.duration, transitionTimingFunction: transaction['webmainline']?.timingFunction }} onClick={ props.WebMainLineonClick } onMouseEnter={ props.WebMainLineonMouseEnter } onMouseOver={ props.WebMainLineonMouseOver } onKeyPress={ props.WebMainLineonKeyPress } onDrag={ props.WebMainLineonDrag } onMouseLeave={ props.WebMainLineonMouseLeave } onMouseUp={ props.WebMainLineonMouseUp } onMouseDown={ props.WebMainLineonMouseDown } onKeyDown={ props.WebMainLineonKeyDown } onChange={ props.WebMainLineonChange } ondelay={ props.WebMainLineondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonefivenigth_five_eightfivethree" className={` instance line ${ props.onClick ? 'cursor' : '' } ${ transaction['line']?.type ? transaction['line']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineStyle , transitionDuration: transaction['line']?.duration, transitionTimingFunction: transaction['line']?.timingFunction }} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>

                      <img id="id_sixzerotwoo_oneonefivenigth_five_eightfivethree_onetwoozerotwoo_onesixsixone" className={` line line ${ props.onClick ? 'cursor' : '' } ${ transaction['line']?.type ? transaction['line']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineStyle , transitionDuration: transaction['line']?.duration, transitionTimingFunction: transaction['line']?.timingFunction }} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay } src={props.Line2 || "" } />

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainstore']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonesixzero" className={` instance webmainstore ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainstore']?.type ? transaction['webmainstore']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebMainStoreStyle , transitionDuration: transaction['webmainstore']?.duration, transitionTimingFunction: transaction['webmainstore']?.timingFunction }} onClick={ props.WebMainStoreonClick } onMouseEnter={ props.WebMainStoreonMouseEnter } onMouseOver={ props.WebMainStoreonMouseOver } onKeyPress={ props.WebMainStoreonKeyPress } onDrag={ props.WebMainStoreonDrag } onMouseLeave={ props.WebMainStoreonMouseLeave } onMouseUp={ props.WebMainStoreonMouseUp } onMouseDown={ props.WebMainStoreonMouseDown } onKeyDown={ props.WebMainStoreonKeyDown } onChange={ props.WebMainStoreonChange } ondelay={ props.WebMainStoreondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['store']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonesixzero_five_eightfourseven" className={` frame store ${ props.onClick ? 'cursor' : '' } ${ transaction['store']?.type ? transaction['store']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.StoreStyle , transitionDuration: transaction['store']?.duration, transitionTimingFunction: transaction['store']?.timingFunction } } onClick={ props.StoreonClick } onMouseEnter={ props.StoreonMouseEnter } onMouseOver={ props.StoreonMouseOver } onKeyPress={ props.StoreonKeyPress } onDrag={ props.StoreonDrag } onMouseLeave={ props.StoreonMouseLeave } onMouseUp={ props.StoreonMouseUp } onMouseDown={ props.StoreonMouseDown } onKeyDown={ props.StoreonKeyDown } onChange={ props.StoreonChange } ondelay={ props.Storeondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconappstore']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonesixzero_five_eightthreetwoo" className={` instance iconappstore ${ props.onClick ? 'cursor' : '' } ${ transaction['iconappstore']?.type ? transaction['iconappstore']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconAppStoreStyle , transitionDuration: transaction['iconappstore']?.duration, transitionTimingFunction: transaction['iconappstore']?.timingFunction }} onClick={ props.IconAppStoreonClick } onMouseEnter={ props.IconAppStoreonMouseEnter } onMouseOver={ props.IconAppStoreonMouseOver } onKeyPress={ props.IconAppStoreonKeyPress } onDrag={ props.IconAppStoreonDrag } onMouseLeave={ props.IconAppStoreonMouseLeave } onMouseUp={ props.IconAppStoreonMouseUp } onMouseDown={ props.IconAppStoreonMouseDown } onKeyDown={ props.IconAppStoreonKeyDown } onChange={ props.IconAppStoreonChange } ondelay={ props.IconAppStoreondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreetwoo_five_eightonethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="136" height="40">
                            <path d="M130.963 40L5.03704 40C2.2163 40 0 37.8 0 35L0 5C0 2.2 2.2163 0 5.03704 0L130.963 0C133.784 0 136 2.2 136 5L136 35C136 37.8 133.784 40 130.963 40Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreetwoo_five_eightonefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20.76123046875" height="25.512893676757812">
                            <path d="M4.34 3.39047C4.3346 2.58604 4.99862 2.19193 5.03101 2.17573C4.65311 1.62505 4.07008 1.54947 3.85954 1.54407C3.36827 1.49008 2.89321 1.841 2.63948 1.841C2.38575 1.841 1.99706 1.54947 1.58138 1.56026C1.04693 1.57106 0.544866 1.8788 0.269543 2.35929C-0.297298 3.34188 0.123784 4.78337 0.669031 5.5716C0.938956 5.96032 1.25747 6.39222 1.67315 6.37603C2.07804 6.35983 2.22919 6.11688 2.72046 6.11688C3.20632 6.11688 3.34668 6.37603 3.77316 6.36523C4.21044 6.35983 4.48576 5.97651 4.74489 5.5824C5.058 5.13969 5.18217 4.69699 5.18756 4.6754C5.18756 4.6754 4.3508 4.35686 4.34 3.39047L4.34 3.39047ZM3.54103 1.02038C3.75696 0.74504 3.90812 0.377919 3.86493 0C3.54642 0.0161965 3.15233 0.221352 2.9256 0.485896C2.72585 0.723444 2.54231 1.10676 2.59089 1.46848C2.94719 1.49548 3.31429 1.29032 3.54103 1.02038L3.54103 1.02038Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreetwoo_five_eightonefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="79.76806640625" height="28.859298706054688">
                            <path d="M2.80721 6.24105L2.24037 6.24105L1.92726 5.26386L0.847563 5.26386L0.550646 6.24105L0 6.24105L1.0689 2.91537L1.73292 2.91537L2.80721 6.24105L2.80721 6.24105ZM1.83549 4.85355L1.55476 3.98434C1.52237 3.89796 1.46839 3.6874 1.38741 3.35807L1.37661 3.35807C1.34422 3.49844 1.29024 3.709 1.22006 3.98434L0.944736 4.85355L1.83549 4.85355L1.83549 4.85355ZM5.55505 5.01012C5.55505 5.42043 5.44168 5.73896 5.22034 5.97651C5.0206 6.18706 4.77766 6.29504 4.48075 6.29504C4.16224 6.29504 3.9355 6.18166 3.79514 5.95491L3.79514 7.21284L3.26609 7.21284L3.26609 4.6268C3.26609 4.37305 3.26069 4.10851 3.24449 3.83857L3.71416 3.83857L3.74655 4.21649L3.75735 4.21649C3.9355 3.93035 4.20542 3.78458 4.56712 3.78458C4.84784 3.78458 5.08538 3.89796 5.27432 4.11931C5.46327 4.35146 5.55505 4.6484 5.55505 5.01012L5.55505 5.01012ZM5.0152 5.03171C5.0152 4.79956 4.96121 4.60521 4.85864 4.45404C4.74527 4.29747 4.58872 4.21649 4.39437 4.21649C4.26481 4.21649 4.14604 4.25968 4.03807 4.34606C3.9301 4.43244 3.85992 4.54582 3.82753 4.69159C3.81133 4.74557 3.80593 4.79956 3.80054 4.85355L3.80054 5.25307C3.80054 5.42583 3.85452 5.57699 3.96249 5.69577C4.07046 5.81454 4.21082 5.87393 4.37818 5.87393C4.57792 5.87393 4.73447 5.79835 4.84784 5.64178C4.96121 5.49061 5.0152 5.28546 5.0152 5.03171ZM8.31907 5.01012C8.31907 5.42043 8.2057 5.73896 7.98436 5.97651C7.78462 6.18706 7.54169 6.29504 7.24477 6.29504C6.92626 6.29504 6.69952 6.18166 6.55916 5.95491L6.55916 7.21284L6.02471 7.21284L6.02471 4.6268C6.02471 4.37305 6.01932 4.10851 6.00312 3.83857L6.47279 3.83857L6.50518 4.21649L6.51598 4.21649C6.69413 3.93035 6.96405 3.78458 7.32575 3.78458C7.60647 3.78458 7.844 3.89796 8.03295 4.11931C8.2219 4.35146 8.31907 4.6484 8.31907 5.01012L8.31907 5.01012ZM7.77382 5.03171C7.77382 4.79956 7.71984 4.60521 7.61727 4.45404C7.5039 4.29747 7.34734 4.21649 7.153 4.21649C7.02343 4.21649 6.90467 4.25968 6.7967 4.34606C6.68873 4.43244 6.61855 4.54582 6.58616 4.69159C6.56996 4.75637 6.55916 4.81036 6.55916 4.85355L6.55916 5.25307C6.55916 5.42583 6.61315 5.57699 6.72112 5.69577C6.82909 5.81454 6.96945 5.87393 7.1368 5.87393C7.33655 5.87393 7.4931 5.79835 7.60647 5.64178C7.71984 5.49061 7.77382 5.28546 7.77382 5.03171ZM11.4016 5.30705C11.4016 5.58779 11.3044 5.81994 11.1047 5.9981C10.8888 6.19246 10.5864 6.28964 10.1977 6.28964C9.84144 6.28964 9.55533 6.21946 9.33399 6.08449L9.45815 5.64178C9.69029 5.78215 9.94942 5.84694 10.2301 5.84694C10.4299 5.84694 10.5864 5.80375 10.6998 5.71197C10.8132 5.62019 10.8672 5.50141 10.8672 5.35024C10.8672 5.21527 10.8186 5.1019 10.7268 5.01012C10.635 4.91834 10.4839 4.83196 10.2679 4.75097C9.68489 4.53502 9.39337 4.21649 9.39337 3.79538C9.39337 3.52004 9.49594 3.29869 9.70109 3.12592C9.90623 2.95316 10.1816 2.86678 10.5163 2.86678C10.8186 2.86678 11.0723 2.92077 11.272 3.02335L11.1371 3.45525C10.9481 3.35268 10.7376 3.30409 10.5001 3.30409C10.3111 3.30409 10.1654 3.35268 10.0574 3.44446C9.97101 3.52544 9.92242 3.62802 9.92242 3.74679C9.92242 3.87636 9.97101 3.98974 10.0736 4.07072C10.16 4.1517 10.3219 4.23269 10.5594 4.32986C10.8456 4.44324 11.0561 4.57821 11.1911 4.73478C11.3368 4.88594 11.4016 5.0749 11.4016 5.30705L11.4016 5.30705ZM13.1669 4.24348L12.5785 4.24348L12.5785 5.40963C12.5785 5.70657 12.6811 5.85233 12.8916 5.85233C12.9888 5.85233 13.0643 5.84694 13.1291 5.82534L13.1453 6.23025C13.0427 6.26805 12.9024 6.28964 12.7296 6.28964C12.5191 6.28964 12.3571 6.22485 12.2384 6.09528C12.1196 5.96571 12.0602 5.74976 12.0602 5.44742L12.0602 4.23808L11.7093 4.23808L11.7093 3.83857L12.0602 3.83857L12.0602 3.40127L12.5839 3.2447L12.5839 3.84397L13.1723 3.84397L13.1669 4.24348L13.1669 4.24348ZM15.8122 5.02092C15.8122 5.38804 15.7042 5.69037 15.4937 5.92792C15.2723 6.17087 14.9808 6.29504 14.6137 6.29504C14.2628 6.29504 13.9821 6.17626 13.7715 5.94412C13.561 5.71197 13.4584 5.41503 13.4584 5.05871C13.4584 4.68619 13.5664 4.38385 13.7823 4.1463C13.9983 3.90876 14.2898 3.78998 14.6515 3.78998C15.0024 3.78998 15.2885 3.90876 15.4991 4.1409C15.7096 4.36766 15.8122 4.66459 15.8122 5.02092ZM15.2615 5.03171C15.2615 4.81036 15.2129 4.6214 15.1212 4.46484C15.0078 4.27588 14.8512 4.1787 14.6407 4.1787C14.4248 4.1787 14.2628 4.27588 14.1494 4.46484C14.0523 4.6214 14.0091 4.81576 14.0091 5.04251C14.0091 5.26386 14.0577 5.45282 14.1494 5.60939C14.2628 5.79835 14.4248 5.89553 14.6353 5.89553C14.8404 5.89553 14.997 5.79835 15.1158 5.60399C15.2129 5.44202 15.2615 5.25306 15.2615 5.03171L15.2615 5.03171ZM17.5451 4.30827C17.4911 4.29747 17.4317 4.29207 17.3777 4.29207C17.1888 4.29207 17.043 4.36226 16.9459 4.50263C16.8595 4.6268 16.8109 4.78337 16.8109 4.97772L16.8109 6.23565L16.2764 6.23565L16.2764 4.59441C16.2764 4.34066 16.271 4.09232 16.2603 3.83857L16.7245 3.83857L16.7461 4.29747L16.7623 4.29747C16.8163 4.14091 16.9081 4.01133 17.0268 3.91955C17.1402 3.83857 17.2752 3.78998 17.4101 3.78998C17.4587 3.78998 17.5019 3.79538 17.5451 3.80078L17.5451 4.30827L17.5451 4.30827ZM19.9312 4.92913C19.9312 5.01012 19.9258 5.0911 19.9096 5.17208L18.3117 5.17208C18.3171 5.40963 18.3927 5.59319 18.5438 5.71736C18.6788 5.83074 18.8515 5.88473 19.0675 5.88473C19.305 5.88473 19.5209 5.84694 19.7153 5.77135L19.7963 6.13847C19.5695 6.23565 19.2996 6.28424 18.9919 6.28424C18.6194 6.28424 18.3279 6.17627 18.1173 5.95491C17.9068 5.73356 17.7988 5.44202 17.7988 5.0749C17.7988 4.71318 17.896 4.41085 18.0957 4.1733C18.3009 3.91415 18.5816 3.78998 18.9325 3.78998C19.278 3.78998 19.5425 3.91955 19.7153 4.1733C19.861 4.37845 19.9312 4.6268 19.9312 4.92913ZM19.4238 4.78876C19.4292 4.6322 19.3914 4.49183 19.3212 4.37845C19.2294 4.22729 19.0891 4.1571 18.8947 4.1571C18.722 4.1571 18.5762 4.22729 18.4682 4.37305C18.3819 4.48643 18.3279 4.6268 18.3117 4.78876L19.4238 4.78876L19.4238 4.78876ZM0.696405 1.73842C0.550646 1.73842 0.421082 1.73303 0.313113 1.71683L0.313113 0.107977C0.46427 0.0863813 0.615428 0.0755837 0.766585 0.0755837C1.37661 0.0755837 1.65734 0.377918 1.65734 0.869212C1.65734 1.43069 1.32263 1.73842 0.696405 1.73842L0.696405 1.73842ZM0.782781 0.280739C0.701804 0.280739 0.631624 0.286138 0.57224 0.296936L0.57224 1.51707C0.604631 1.52247 0.664014 1.52247 0.75039 1.52247C1.14988 1.52247 1.37661 1.29572 1.37661 0.869212C1.37661 0.491294 1.17147 0.280739 0.782781 0.280739ZM2.53189 1.74922C2.18639 1.74922 1.96505 1.49008 1.96505 1.14455C1.96505 0.782831 2.19179 0.523687 2.55349 0.523687C2.89359 0.523687 3.12033 0.766634 3.12033 1.12836C3.12033 1.49008 2.88279 1.74922 2.53189 1.74922ZM2.54269 0.712646C2.35374 0.712646 2.22958 0.890808 2.22958 1.13915C2.22958 1.3821 2.35374 1.56026 2.53729 1.56026C2.72084 1.56026 2.845 1.3713 2.845 1.13375C2.8504 0.890808 2.72624 0.712646 2.54269 0.712646L2.54269 0.712646ZM5.08538 0.545282L4.71828 1.72223L4.47535 1.72223L4.32419 1.20934C4.2864 1.08517 4.25401 0.955593 4.22702 0.826021L4.22162 0.826021C4.20003 0.955593 4.16763 1.08517 4.12445 1.20934L3.96249 1.72223L3.71956 1.72223L3.37406 0.545282L3.64398 0.545282L3.77894 1.10676C3.81133 1.24173 3.83833 1.3659 3.85992 1.48468L3.86532 1.48468C3.88691 1.3875 3.9139 1.26333 3.96249 1.10676L4.12984 0.545282L4.34578 0.545282L4.50774 1.09596C4.54553 1.23093 4.57792 1.36051 4.60491 1.48468L4.61031 1.48468C4.6265 1.3659 4.6535 1.23633 4.69129 1.09596L4.83705 0.545282L5.08538 0.545282L5.08538 0.545282ZM6.4404 1.72223L6.18127 1.72223L6.18127 1.04737C6.18127 0.836819 6.10029 0.734241 5.94374 0.734241C5.78718 0.734241 5.67921 0.869212 5.67921 1.02038L5.67921 1.72223L5.41468 1.72223L5.41468 0.88001C5.41468 0.777432 5.41468 0.664057 5.40389 0.545282L5.63602 0.545282L5.64682 0.728842L5.65222 0.728842C5.7224 0.604669 5.86816 0.523687 6.02471 0.523687C6.27304 0.523687 6.435 0.712646 6.435 1.02038L6.4404 1.72223L6.4404 1.72223ZM7.1638 1.72223L6.89927 1.72223L6.89927 0L7.1638 0L7.1638 1.72223ZM8.11933 1.74922C7.77382 1.74922 7.55249 1.49008 7.55249 1.14455C7.55249 0.782831 7.77922 0.523687 8.14092 0.523687C8.48103 0.523687 8.70776 0.766634 8.70776 1.12836C8.70776 1.49008 8.47023 1.74922 8.11933 1.74922ZM8.13013 0.712646C7.94118 0.712646 7.81701 0.890808 7.81701 1.13915C7.81701 1.3821 7.94118 1.56026 8.12473 1.56026C8.30828 1.56026 8.43244 1.3713 8.43244 1.13375C8.43244 0.890808 8.31367 0.712646 8.13013 0.712646L8.13013 0.712646ZM9.73887 1.72223L9.71728 1.58726L9.71188 1.58726C9.6309 1.69523 9.51754 1.74922 9.36638 1.74922C9.15584 1.74922 9.00468 1.60345 9.00468 1.4037C9.00468 1.11216 9.25841 0.960992 9.69569 0.960992L9.69569 0.939397C9.69569 0.782831 9.61471 0.707247 9.45275 0.707247C9.33399 0.707247 9.23681 0.734241 9.14504 0.793629L9.09106 0.620866C9.19903 0.550681 9.33399 0.518288 9.49594 0.518288C9.80366 0.518288 9.96021 0.680253 9.96021 1.00418L9.96021 1.43609C9.96021 1.55486 9.96561 1.64664 9.97641 1.71683L9.73887 1.72223L9.73887 1.72223ZM9.70109 1.13375C9.40957 1.13375 9.26381 1.20394 9.26381 1.3713C9.26381 1.49548 9.33939 1.55486 9.44196 1.55486C9.57692 1.55486 9.70109 1.45229 9.70109 1.31732L9.70109 1.13375ZM11.2289 1.72223L11.2181 1.53327L11.2127 1.53327C11.1371 1.67364 11.0129 1.74922 10.8348 1.74922C10.5486 1.74922 10.3381 1.50088 10.3381 1.14995C10.3381 0.782831 10.554 0.523687 10.8564 0.523687C11.0129 0.523687 11.1263 0.577675 11.1911 0.685652L11.1965 0.685652L11.1965 0L11.461 0L11.461 1.4037C11.461 1.51707 11.4664 1.62505 11.4718 1.72223L11.2289 1.72223L11.2289 1.72223ZM11.1911 1.03118C11.1911 0.863813 11.0831 0.723444 10.9157 0.723444C10.7214 0.723444 10.6026 0.896206 10.6026 1.13915C10.6026 1.3767 10.7268 1.53867 10.9103 1.53867C11.0777 1.53867 11.1911 1.3929 11.1911 1.22554L11.1911 1.03118L11.1911 1.03118ZM13.1183 1.74922C12.7728 1.74922 12.5515 1.49008 12.5515 1.14455C12.5515 0.782831 12.7782 0.523687 13.1399 0.523687C13.48 0.523687 13.7068 0.766634 13.7068 1.12836C13.7068 1.49008 13.4692 1.74922 13.1183 1.74922ZM13.1291 0.712646C12.9402 0.712646 12.8214 0.890808 12.8214 1.13915C12.8214 1.3821 12.9456 1.56026 13.1291 1.56026C13.3127 1.56026 13.4368 1.3713 13.4368 1.13375C13.4314 0.890808 13.3127 0.712646 13.1291 0.712646ZM15.1158 1.72223L14.8512 1.72223L14.8512 1.04737C14.8512 0.836819 14.7703 0.734241 14.6137 0.734241C14.4572 0.734241 14.3546 0.869212 14.3546 1.02038L14.3546 1.72223L14.0901 1.72223L14.0901 0.88001C14.0901 0.777432 14.0901 0.664057 14.0793 0.545282L14.3114 0.545282L14.3222 0.728842L14.3276 0.728842C14.3978 0.604669 14.5435 0.523687 14.7001 0.523687C14.9484 0.523687 15.1104 0.712646 15.1104 1.02038L15.1104 1.72223L15.1158 1.72223ZM16.8757 0.73964L16.5896 0.73964L16.5896 1.31192C16.5896 1.45768 16.6381 1.52787 16.7407 1.52787C16.7893 1.52787 16.8271 1.52247 16.8595 1.51707L16.8649 1.71683C16.8163 1.73842 16.7461 1.74382 16.6597 1.74382C16.4546 1.74382 16.3304 1.63045 16.3304 1.33351L16.3304 0.73964L16.1577 0.73964L16.1577 0.545282L16.3304 0.545282L16.3304 0.329329L16.5896 0.253745L16.5896 0.545282L16.8757 0.545282L16.8757 0.73964ZM18.2631 1.72223L18.004 1.72223L18.004 1.05277C18.004 0.842218 17.923 0.734241 17.7664 0.734241C17.6315 0.734241 17.5019 0.826021 17.5019 1.01498L17.5019 1.72763L17.2428 1.72763L17.2428 0L17.5019 0L17.5019 0.707247L17.5073 0.707247C17.5883 0.577675 17.707 0.512889 17.8636 0.512889C18.1119 0.512889 18.2631 0.707247 18.2631 1.01498L18.2631 1.72223L18.2631 1.72223ZM19.6829 1.19854L18.8947 1.19854C18.9001 1.41989 19.0459 1.54947 19.2672 1.54947C19.386 1.54947 19.4885 1.52787 19.5857 1.49548L19.6289 1.67904C19.5155 1.72763 19.386 1.74922 19.2348 1.74922C18.8677 1.74922 18.6464 1.51707 18.6464 1.15535C18.6464 0.793629 18.8677 0.523687 19.2024 0.523687C19.5047 0.523687 19.6937 0.745039 19.6937 1.08517C19.6937 1.11756 19.6937 1.15535 19.6829 1.19854ZM19.4454 1.00958C19.4454 0.826022 19.3536 0.69645 19.1862 0.69645C19.0351 0.69645 18.9163 0.826022 18.9001 1.00958L19.4454 1.00958L19.4454 1.00958Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icongoogleplay']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonesixzero_five_eightthreeone" className={` instance icongoogleplay ${ props.onClick ? 'cursor' : '' } ${ transaction['icongoogleplay']?.type ? transaction['icongoogleplay']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconGooglePlayStyle , transitionDuration: transaction['icongoogleplay']?.duration, transitionTimingFunction: transaction['icongoogleplay']?.timingFunction }} onClick={ props.IconGooglePlayonClick } onMouseEnter={ props.IconGooglePlayonMouseEnter } onMouseOver={ props.IconGooglePlayonMouseOver } onKeyPress={ props.IconGooglePlayonKeyPress } onDrag={ props.IconGooglePlayonDrag } onMouseLeave={ props.IconGooglePlayonMouseLeave } onMouseUp={ props.IconGooglePlayonMouseUp } onMouseDown={ props.IconGooglePlayonMouseDown } onKeyDown={ props.IconGooglePlayonKeyDown } onChange={ props.IconGooglePlayonChange } ondelay={ props.IconGooglePlayondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eightoneseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="136" height="40">
                            <path d="M130.963 40L5.03704 40C2.2163 40 0 37.8 0 35L0 5C0 2.2 2.2163 0 5.03704 0L130.963 0C133.784 0 136 2.2 136 5L136 35C136 37.8 133.784 40 130.963 40Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eightoneeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="38.970703125" height="6.1978302001953125">
                            <path d="M6.02636 3.29884C6.02636 4.09856 5.82548 4.79832 5.32329 5.29814C4.72065 5.89793 4.01757 6.19783 3.11362 6.19783C2.20967 6.19783 1.50659 5.89793 0.903952 5.29814C0.301316 4.69835 0 3.9986 0 3.09891C0 2.19923 0.301316 1.49947 0.903952 0.899685C1.50659 0.299895 2.20967 0 3.11362 0C3.51538 0 3.91714 0.0999647 4.31889 0.299895C4.72065 0.499825 5.02197 0.699755 5.22285 0.99965L4.72065 1.49947C4.31889 0.999649 3.8167 0.79972 3.11362 0.79972C2.51098 0.79972 1.90835 0.999649 1.50659 1.49947C1.00439 1.89933 0.803514 2.49912 0.803514 3.19888C0.803514 3.89863 1.00439 4.49842 1.50659 4.89828C2.00879 5.29814 2.51098 5.59804 3.11362 5.59804C3.8167 5.59804 4.31889 5.39811 4.82109 4.89828C5.12241 4.59839 5.32329 4.19853 5.32329 3.6987L3.11362 3.6987L3.11362 2.89898L6.02636 2.89898L6.02636 3.29884L6.02636 3.29884ZM10.6466 0.79972L7.93471 0.79972L7.93471 2.69905L10.4457 2.69905L10.4457 3.39881L7.93471 3.39881L7.93471 5.29814L10.6466 5.29814L10.6466 6.09786L7.1312 6.09786L7.1312 0.0999649L10.6466 0.0999649L10.6466 0.79972ZM13.9611 6.09786L13.1576 6.09786L13.1576 0.79972L11.4501 0.79972L11.4501 0.0999649L15.6685 0.0999649L15.6685 0.79972L13.9611 0.79972L13.9611 6.09786ZM18.5813 6.09786L18.5813 0.0999649L19.3848 0.0999649L19.3848 6.09786L18.5813 6.09786ZM22.7997 6.09786L21.9962 6.09786L21.9962 0.79972L20.2888 0.79972L20.2888 0.0999649L24.4068 0.0999649L24.4068 0.79972L22.6993 0.79972L22.6993 6.09786L22.7997 6.09786ZM32.3415 5.29814C31.7388 5.89793 31.0358 6.19783 30.1318 6.19783C29.2279 6.19783 28.5248 5.89793 27.9221 5.29814C27.3195 4.69835 27.0182 3.9986 27.0182 3.09891C27.0182 2.19923 27.3195 1.49947 27.9221 0.899685C28.5248 0.299895 29.2279 0 30.1318 0C31.0358 0 31.7388 0.299895 32.3415 0.899685C32.9441 1.49947 33.2454 2.19923 33.2454 3.09891C33.2454 3.9986 32.9441 4.69835 32.3415 5.29814ZM28.5248 4.79832C28.9265 5.19818 29.5292 5.49807 30.1318 5.49807C30.7345 5.49807 31.3371 5.29814 31.7388 4.79832C32.1406 4.39846 32.4419 3.79867 32.4419 3.09891C32.4419 2.39916 32.241 1.79937 31.7388 1.39951C31.3371 0.99965 30.7345 0.699755 30.1318 0.699755C29.5292 0.699755 28.9265 0.899685 28.5248 1.39951C28.123 1.79937 27.8217 2.39916 27.8217 3.09891C27.8217 3.79867 28.0226 4.39846 28.5248 4.79832ZM34.3503 6.09786L34.3503 0.0999649L35.2542 0.0999649L38.167 4.79832L38.167 0.0999649L38.9705 0.0999649L38.9705 6.09786L38.167 6.09786L35.0533 1.19958L35.0533 6.09786L34.3503 6.09786L34.3503 6.09786Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eightonenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="84.9716796875" height="16.894088745117188">
                            <path d="M27.1186 4.69836C24.7081 4.69836 22.7997 6.49773 22.7997 8.99685C22.7997 11.396 24.7081 13.2953 27.1186 13.2953C29.5292 13.2953 31.4375 11.496 31.4375 8.99685C31.4375 6.39776 29.5292 4.69836 27.1186 4.69836ZM27.1186 11.496C25.8129 11.496 24.7081 10.3964 24.7081 8.89689C24.7081 7.39741 25.8129 6.2978 27.1186 6.2978C28.4243 6.2978 29.5292 7.29745 29.5292 8.89689C29.5292 10.3964 28.4243 11.496 27.1186 11.496ZM17.7778 4.69836C15.3672 4.69836 13.4589 6.49773 13.4589 8.99685C13.4589 11.396 15.3672 13.2953 17.7778 13.2953C20.1883 13.2953 22.0967 11.496 22.0967 8.99685C22.0967 6.39776 20.1883 4.69836 17.7778 4.69836ZM17.7778 11.496C16.4721 11.496 15.3672 10.3964 15.3672 8.89689C15.3672 7.39741 16.4721 6.2978 17.7778 6.2978C19.0835 6.2978 20.1883 7.29745 20.1883 8.89689C20.1883 10.3964 19.0835 11.496 17.7778 11.496ZM6.629 5.9979L6.629 7.79727L10.9479 7.79727C10.8475 8.79692 10.4457 9.59664 9.9435 10.0965C9.34086 10.6963 8.33647 11.396 6.629 11.396C3.91714 11.396 1.90835 9.29675 1.90835 6.59769C1.90835 3.89864 4.01758 1.79937 6.629 1.79937C8.03515 1.79937 9.13998 2.39916 9.9435 3.09892L11.2492 1.79937C10.1444 0.799719 8.73823 0 6.72944 0C3.11362 0 0 2.99895 0 6.59769C0 10.1964 3.11362 13.1954 6.72944 13.1954C8.73823 13.1954 10.1444 12.5956 11.3497 11.296C12.5549 10.0965 12.9567 8.39706 12.9567 7.09752C12.9567 6.69766 12.9567 6.2978 12.8562 5.9979L6.629 5.9979L6.629 5.9979ZM52.2285 7.39741C51.8267 6.39776 50.8223 4.69836 48.6127 4.69836C46.403 4.69836 44.5951 6.39776 44.5951 8.99685C44.5951 11.396 46.403 13.2953 48.8135 13.2953C50.7219 13.2953 51.9272 12.0958 52.3289 11.396L50.9228 10.3964C50.4206 11.0961 49.8179 11.5959 48.8135 11.5959C47.8091 11.5959 47.2065 11.1961 46.7043 10.2964L52.4294 7.89724L52.2285 7.39741L52.2285 7.39741ZM46.403 8.79692C46.403 7.19748 47.7087 6.2978 48.6127 6.2978C49.3157 6.2978 50.0188 6.69766 50.2197 7.19748L46.403 8.79692ZM41.6823 12.8955L43.5907 12.8955L43.5907 0.39986L41.6823 0.39986L41.6823 12.8955ZM38.6692 5.59804C38.167 5.09822 37.3634 4.59839 36.3591 4.59839C34.2498 4.59839 32.241 6.49773 32.241 8.89689C32.241 11.296 34.1494 13.0954 36.3591 13.0954C37.3634 13.0954 38.167 12.5956 38.5687 12.0958L38.6692 12.0958L38.6692 12.6956C38.6692 14.295 37.7652 15.1947 36.3591 15.1947C35.2542 15.1947 34.4507 14.395 34.2498 13.6952L32.6428 14.395C33.145 15.4946 34.3503 16.8941 36.4595 16.8941C38.6692 16.8941 40.4771 15.5945 40.4771 12.4956L40.4771 4.89829L38.6692 4.89829L38.6692 5.59804L38.6692 5.59804ZM36.4595 11.496C35.1538 11.496 34.0489 10.3964 34.0489 8.89689C34.0489 7.39741 35.1538 6.2978 36.4595 6.2978C37.7652 6.2978 38.7696 7.39741 38.7696 8.89689C38.7696 10.3964 37.7652 11.496 36.4595 11.496ZM60.9667 0.39986L56.4469 0.39986L56.4469 12.8955L58.3553 12.8955L58.3553 8.19713L60.9667 8.19713C63.0759 8.19713 65.0847 6.69766 65.0847 4.2985C65.0847 1.89933 63.0759 0.39986 60.9667 0.39986L60.9667 0.39986ZM61.0671 6.39776L58.3553 6.39776L58.3553 2.09927L61.0671 2.09927C62.4733 2.09927 63.2768 3.29884 63.2768 4.19853C63.1764 5.29815 62.3729 6.39776 61.0671 6.39776ZM72.6177 4.59839C71.2115 4.59839 69.8054 5.19818 69.3032 6.49773L71.0106 7.19748C71.4124 6.49773 72.015 6.2978 72.7181 6.2978C73.7225 6.2978 74.6264 6.89759 74.7269 7.89724L74.7269 7.9972C74.4256 7.79727 73.6221 7.49738 72.8185 7.49738C71.0106 7.49738 69.2027 8.49703 69.2027 10.2964C69.2027 11.9958 70.7093 13.0954 72.3163 13.0954C73.6221 13.0954 74.2247 12.4956 74.7269 11.8958L74.8273 11.8958L74.8273 12.8955L76.6352 12.8955L76.6352 8.09717C76.4344 5.89794 74.7269 4.59839 72.6177 4.59839L72.6177 4.59839ZM72.4168 11.496C71.8141 11.496 70.9102 11.1961 70.9102 10.3964C70.9102 9.39671 72.015 9.09682 72.919 9.09682C73.7225 9.09682 74.1243 9.29675 74.6265 9.49668C74.4256 10.6963 73.4212 11.496 72.4168 11.496L72.4168 11.496ZM82.9629 4.89829L80.8537 10.2964L80.7533 10.2964L78.5436 4.89829L76.5348 4.89829L79.8493 12.4956L77.9409 16.6942L79.8493 16.6942L84.9717 4.89829L82.9629 4.89829L82.9629 4.89829ZM66.0891 12.8955L67.9975 12.8955L67.9975 0.39986L66.0891 0.39986L66.0891 12.8955Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                          <div id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwoozero" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwooone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="13.05712890625" height="24.991249084472656">
                                <path d="M0.401757 0C0.100439 0.299895 0 0.79972 0 1.39951L0 23.4918C0 24.0916 0.200879 24.5914 0.502197 24.8913L0.602637 24.9913L13.0571 12.5956L13.0571 12.3957L0.401757 0Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwootwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="10.19482421875" height="8.497024536132812">
                                <path d="M4.11802 8.49702L0 4.39846L0 4.09856L4.11802 0L4.21846 0.0999644L9.13999 2.89898C10.5461 3.6987 10.5461 4.99825 9.13999 5.79797L4.11802 8.49702L4.11802 8.49702Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwoothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16.7734375" height="12.884048461914062">
                                <path d="M16.7734 4.19853L12.5549 0L0 12.4956C0.502197 12.9955 1.20527 12.9955 2.10923 12.5956L16.7734 4.19853" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwoofour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16.7734375" height="12.824241638183594">
                                <path d="M16.7734 8.62571L2.10923 0.32861C1.20527 -0.171215 0.502197 -0.0712498 0 0.428575L12.5549 12.8242L16.7734 8.62571L16.7734 8.62571Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwoofive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16.6728515625" height="8.7354736328125">
                                <path d="M16.6729 0L2.10923 8.19713C1.30571 8.69696 0.602637 8.59699 0.10044 8.19713L0 8.2971L0.10044 8.39706C0.602637 8.79692 1.30571 8.89689 2.10923 8.39706L16.6729 0Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwoosix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="23.10107421875" height="12.395660400390625">
                                <path d="M0.401757 12.2957C0.100439 11.9958 0 11.496 0 10.8962L0 10.9962C0 11.5959 0.200879 12.0958 0.502197 12.3957L0.502197 12.2957L0.401757 12.2957ZM22.0967 1.29954L17.0747 4.09857L17.1751 4.19853L22.0967 1.39951C22.7998 0.99965 23.1011 0.499825 23.1011 0C23.1011 0.499825 22.6993 0.899684 22.0967 1.29954L22.0967 1.29954Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                              <svg id="id_sixzerotwoo_oneonesixzero_five_eightthreeone_five_eighttwooseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="23.10107421875" height="12.848320007324219">
                                <path d="M2.51099 0.452651L22.0967 11.5488C22.6993 11.9486 23.1011 12.3485 23.1011 12.8483C23.1011 12.3485 22.7998 11.8487 22.0967 11.4488L2.51099 0.352686C1.10483 -0.447034 0 0.152756 0 1.7522L0 1.85216C0 0.352686 1.10483 -0.347069 2.51099 0.452651Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['webmainfooter']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonesixone" className={` instance webmainfooter ${ props.onClick ? 'cursor' : '' } ${ transaction['webmainfooter']?.type ? transaction['webmainfooter']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.WebMainFooterStyle , transitionDuration: transaction['webmainfooter']?.duration, transitionTimingFunction: transaction['webmainfooter']?.timingFunction }} onClick={ props.WebMainFooteronClick } onMouseEnter={ props.WebMainFooteronMouseEnter } onMouseOver={ props.WebMainFooteronMouseOver } onKeyPress={ props.WebMainFooteronKeyPress } onDrag={ props.WebMainFooteronDrag } onMouseLeave={ props.WebMainFooteronMouseLeave } onMouseUp={ props.WebMainFooteronMouseUp } onMouseDown={ props.WebMainFooteronMouseDown } onKeyDown={ props.WebMainFooteronKeyDown } onChange={ props.WebMainFooteronChange } ondelay={ props.WebMainFooterondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonesixone_four_fivefivefive" className={` frame text ${ props.onClick ? 'cursor' : '' } ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction } } onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonesixone_three_fournigth"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text4 || `Y.`}</span>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonesixone_four_fivefivefour"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text5 || `Match Group, LLC c/o Buddy Loomis 8750 N. Central Expressway, Suite 1400 Dallas, TX 75231`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonesixone_four_fivethreeeight" className={` frame text ${ props.onClick ? 'cursor' : '' } ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction } } onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonesixone_four_fivetwoonigth"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text7 || `Politica de privacidad`}</span>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonesixone_four_fivethreezero"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text8 || `Términos y condiciones`}</span>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                      <span id="id_sixzerotwoo_oneonesixone_four_fivefourtwoo"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text9 || `DMCA`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

                  <span id="id_sixzerotwoo_oneonesixone_four_fivefivethree"  className={` text text    ${ props.onClick ? 'cursor' : ''}  ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextStyle , transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay } >{props.Text11 || `© 2023 Concert Plaza, All Rights Reserved`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['spacing']?.animationClass || {}}>

              <div id="id_sixzerotwoo_oneonesixtwoo" className={` instance spacing ${ props.onClick ? 'cursor' : '' } ${ transaction['spacing']?.type ? transaction['spacing']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SpacingStyle , transitionDuration: transaction['spacing']?.duration, transitionTimingFunction: transaction['spacing']?.timingFunction }} onClick={ props.SpacingonClick } onMouseEnter={ props.SpacingonMouseEnter } onMouseOver={ props.SpacingonMouseOver } onKeyPress={ props.SpacingonKeyPress } onDrag={ props.SpacingonDrag } onMouseLeave={ props.SpacingonMouseLeave } onMouseUp={ props.SpacingonMouseUp } onMouseDown={ props.SpacingonMouseDown } onKeyDown={ props.SpacingonKeyDown } onChange={ props.SpacingonChange } ondelay={ props.Spacingondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['spacing']?.animationClass || {}}>

                  <div id="id_sixzerotwoo_oneonesixtwoo_onetwoozerofive_threezeronigthsix" className={` frame spacing ${ props.onClick ? 'cursor' : '' } ${ transaction['spacing']?.type ? transaction['spacing']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SpacingStyle , transitionDuration: transaction['spacing']?.duration, transitionTimingFunction: transaction['spacing']?.timingFunction } } onClick={ props.SpacingonClick } onMouseEnter={ props.SpacingonMouseEnter } onMouseOver={ props.SpacingonMouseOver } onKeyPress={ props.SpacingonKeyPress } onDrag={ props.SpacingonDrag } onMouseLeave={ props.SpacingonMouseLeave } onMouseUp={ props.SpacingonMouseUp } onMouseDown={ props.SpacingonMouseDown } onKeyDown={ props.SpacingonKeyDown } onChange={ props.SpacingonChange } ondelay={ props.Spacingondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['xlinefourzerozero']?.animationClass || {}}>

                      <div id="id_sixzerotwoo_oneonesixtwoo_onetwoozerofive_threezeronigthseven" className={` instance xlinefourzerozero ${ props.onClick ? 'cursor' : '' } ${ transaction['xlinefourzerozero']?.type ? transaction['xlinefourzerozero']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.XLinefourzerozeroStyle , transitionDuration: transaction['xlinefourzerozero']?.duration, transitionTimingFunction: transaction['xlinefourzerozero']?.timingFunction }} onClick={ props.XLinefourzerozeroonClick } onMouseEnter={ props.XLinefourzerozeroonMouseEnter } onMouseOver={ props.XLinefourzerozeroonMouseOver } onKeyPress={ props.XLinefourzerozeroonKeyPress } onDrag={ props.XLinefourzerozeroonDrag } onMouseLeave={ props.XLinefourzerozeroonMouseLeave } onMouseUp={ props.XLinefourzerozeroonMouseUp } onMouseDown={ props.XLinefourzerozeroonMouseDown } onKeyDown={ props.XLinefourzerozeroonKeyDown } onChange={ props.XLinefourzerozeroonChange } ondelay={ props.XLinefourzerozeroondelay }>

                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

VerificationEmail.propTypes = {
    style: PropTypes.any,
Line0: PropTypes.any,
Text0: PropTypes.any,
Text1: PropTypes.any,
Text2: PropTypes.any,
VerificatuEmail0: PropTypes.any,
Text3: PropTypes.any,
Line2: PropTypes.any,
Text4: PropTypes.any,
Text5: PropTypes.any,
Text7: PropTypes.any,
Text8: PropTypes.any,
Text9: PropTypes.any,
Text11: PropTypes.any,
VerificationEmailonClick: PropTypes.any,
VerificationEmailonMouseEnter: PropTypes.any,
VerificationEmailonMouseOver: PropTypes.any,
VerificationEmailonKeyPress: PropTypes.any,
VerificationEmailonDrag: PropTypes.any,
VerificationEmailonMouseLeave: PropTypes.any,
VerificationEmailonMouseUp: PropTypes.any,
VerificationEmailonMouseDown: PropTypes.any,
VerificationEmailonKeyDown: PropTypes.any,
VerificationEmailonChange: PropTypes.any,
VerificationEmailondelay: PropTypes.any,
BoxonClick: PropTypes.any,
BoxonMouseEnter: PropTypes.any,
BoxonMouseOver: PropTypes.any,
BoxonKeyPress: PropTypes.any,
BoxonDrag: PropTypes.any,
BoxonMouseLeave: PropTypes.any,
BoxonMouseUp: PropTypes.any,
BoxonMouseDown: PropTypes.any,
BoxonKeyDown: PropTypes.any,
BoxonChange: PropTypes.any,
Boxondelay: PropTypes.any,
WebMainHeaderonClick: PropTypes.any,
WebMainHeaderonMouseEnter: PropTypes.any,
WebMainHeaderonMouseOver: PropTypes.any,
WebMainHeaderonKeyPress: PropTypes.any,
WebMainHeaderonDrag: PropTypes.any,
WebMainHeaderonMouseLeave: PropTypes.any,
WebMainHeaderonMouseUp: PropTypes.any,
WebMainHeaderonMouseDown: PropTypes.any,
WebMainHeaderonKeyDown: PropTypes.any,
WebMainHeaderonChange: PropTypes.any,
WebMainHeaderondelay: PropTypes.any,
GrouponClick: PropTypes.any,
GrouponMouseEnter: PropTypes.any,
GrouponMouseOver: PropTypes.any,
GrouponKeyPress: PropTypes.any,
GrouponDrag: PropTypes.any,
GrouponMouseLeave: PropTypes.any,
GrouponMouseUp: PropTypes.any,
GrouponMouseDown: PropTypes.any,
GrouponKeyDown: PropTypes.any,
GrouponChange: PropTypes.any,
Groupondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
WebMainLineonClick: PropTypes.any,
WebMainLineonMouseEnter: PropTypes.any,
WebMainLineonMouseOver: PropTypes.any,
WebMainLineonKeyPress: PropTypes.any,
WebMainLineonDrag: PropTypes.any,
WebMainLineonMouseLeave: PropTypes.any,
WebMainLineonMouseUp: PropTypes.any,
WebMainLineonMouseDown: PropTypes.any,
WebMainLineonKeyDown: PropTypes.any,
WebMainLineonChange: PropTypes.any,
WebMainLineondelay: PropTypes.any,
LineonClick: PropTypes.any,
LineonMouseEnter: PropTypes.any,
LineonMouseOver: PropTypes.any,
LineonKeyPress: PropTypes.any,
LineonDrag: PropTypes.any,
LineonMouseLeave: PropTypes.any,
LineonMouseUp: PropTypes.any,
LineonMouseDown: PropTypes.any,
LineonKeyDown: PropTypes.any,
LineonChange: PropTypes.any,
Lineondelay: PropTypes.any,
WebTextTitleSubtitleonClick: PropTypes.any,
WebTextTitleSubtitleonMouseEnter: PropTypes.any,
WebTextTitleSubtitleonMouseOver: PropTypes.any,
WebTextTitleSubtitleonKeyPress: PropTypes.any,
WebTextTitleSubtitleonDrag: PropTypes.any,
WebTextTitleSubtitleonMouseLeave: PropTypes.any,
WebTextTitleSubtitleonMouseUp: PropTypes.any,
WebTextTitleSubtitleonMouseDown: PropTypes.any,
WebTextTitleSubtitleonKeyDown: PropTypes.any,
WebTextTitleSubtitleonChange: PropTypes.any,
WebTextTitleSubtitleondelay: PropTypes.any,
TextonClick: PropTypes.any,
TextonMouseEnter: PropTypes.any,
TextonMouseOver: PropTypes.any,
TextonKeyPress: PropTypes.any,
TextonDrag: PropTypes.any,
TextonMouseLeave: PropTypes.any,
TextonMouseUp: PropTypes.any,
TextonMouseDown: PropTypes.any,
TextonKeyDown: PropTypes.any,
TextonChange: PropTypes.any,
Textondelay: PropTypes.any,
WebTextLonClick: PropTypes.any,
WebTextLonMouseEnter: PropTypes.any,
WebTextLonMouseOver: PropTypes.any,
WebTextLonKeyPress: PropTypes.any,
WebTextLonDrag: PropTypes.any,
WebTextLonMouseLeave: PropTypes.any,
WebTextLonMouseUp: PropTypes.any,
WebTextLonMouseDown: PropTypes.any,
WebTextLonKeyDown: PropTypes.any,
WebTextLonChange: PropTypes.any,
WebTextLondelay: PropTypes.any,
WebMainButton_OneonClick: PropTypes.any,
WebMainButton_OneonMouseEnter: PropTypes.any,
WebMainButton_OneonMouseOver: PropTypes.any,
WebMainButton_OneonKeyPress: PropTypes.any,
WebMainButton_OneonDrag: PropTypes.any,
WebMainButton_OneonMouseLeave: PropTypes.any,
WebMainButton_OneonMouseUp: PropTypes.any,
WebMainButton_OneonMouseDown: PropTypes.any,
WebMainButton_OneonKeyDown: PropTypes.any,
WebMainButton_OneonChange: PropTypes.any,
WebMainButton_Oneondelay: PropTypes.any,
ButtononClick: PropTypes.any,
ButtononMouseEnter: PropTypes.any,
ButtononMouseOver: PropTypes.any,
ButtononKeyPress: PropTypes.any,
ButtononDrag: PropTypes.any,
ButtononMouseLeave: PropTypes.any,
ButtononMouseUp: PropTypes.any,
ButtononMouseDown: PropTypes.any,
ButtononKeyDown: PropTypes.any,
ButtononChange: PropTypes.any,
Buttonondelay: PropTypes.any,
VerificatuEmailonClick: PropTypes.any,
VerificatuEmailonMouseEnter: PropTypes.any,
VerificatuEmailonMouseOver: PropTypes.any,
VerificatuEmailonKeyPress: PropTypes.any,
VerificatuEmailonDrag: PropTypes.any,
VerificatuEmailonMouseLeave: PropTypes.any,
VerificatuEmailonMouseUp: PropTypes.any,
VerificatuEmailonMouseDown: PropTypes.any,
VerificatuEmailonKeyDown: PropTypes.any,
VerificatuEmailonChange: PropTypes.any,
VerificatuEmailondelay: PropTypes.any,
WebTextMonClick: PropTypes.any,
WebTextMonMouseEnter: PropTypes.any,
WebTextMonMouseOver: PropTypes.any,
WebTextMonKeyPress: PropTypes.any,
WebTextMonDrag: PropTypes.any,
WebTextMonMouseLeave: PropTypes.any,
WebTextMonMouseUp: PropTypes.any,
WebTextMonMouseDown: PropTypes.any,
WebTextMonKeyDown: PropTypes.any,
WebTextMonChange: PropTypes.any,
WebTextMondelay: PropTypes.any,
WebMainStoreonClick: PropTypes.any,
WebMainStoreonMouseEnter: PropTypes.any,
WebMainStoreonMouseOver: PropTypes.any,
WebMainStoreonKeyPress: PropTypes.any,
WebMainStoreonDrag: PropTypes.any,
WebMainStoreonMouseLeave: PropTypes.any,
WebMainStoreonMouseUp: PropTypes.any,
WebMainStoreonMouseDown: PropTypes.any,
WebMainStoreonKeyDown: PropTypes.any,
WebMainStoreonChange: PropTypes.any,
WebMainStoreondelay: PropTypes.any,
StoreonClick: PropTypes.any,
StoreonMouseEnter: PropTypes.any,
StoreonMouseOver: PropTypes.any,
StoreonKeyPress: PropTypes.any,
StoreonDrag: PropTypes.any,
StoreonMouseLeave: PropTypes.any,
StoreonMouseUp: PropTypes.any,
StoreonMouseDown: PropTypes.any,
StoreonKeyDown: PropTypes.any,
StoreonChange: PropTypes.any,
Storeondelay: PropTypes.any,
IconAppStoreonClick: PropTypes.any,
IconAppStoreonMouseEnter: PropTypes.any,
IconAppStoreonMouseOver: PropTypes.any,
IconAppStoreonKeyPress: PropTypes.any,
IconAppStoreonDrag: PropTypes.any,
IconAppStoreonMouseLeave: PropTypes.any,
IconAppStoreonMouseUp: PropTypes.any,
IconAppStoreonMouseDown: PropTypes.any,
IconAppStoreonKeyDown: PropTypes.any,
IconAppStoreonChange: PropTypes.any,
IconAppStoreondelay: PropTypes.any,
IconGooglePlayonClick: PropTypes.any,
IconGooglePlayonMouseEnter: PropTypes.any,
IconGooglePlayonMouseOver: PropTypes.any,
IconGooglePlayonKeyPress: PropTypes.any,
IconGooglePlayonDrag: PropTypes.any,
IconGooglePlayonMouseLeave: PropTypes.any,
IconGooglePlayonMouseUp: PropTypes.any,
IconGooglePlayonMouseDown: PropTypes.any,
IconGooglePlayonKeyDown: PropTypes.any,
IconGooglePlayonChange: PropTypes.any,
IconGooglePlayondelay: PropTypes.any,
WebMainFooteronClick: PropTypes.any,
WebMainFooteronMouseEnter: PropTypes.any,
WebMainFooteronMouseOver: PropTypes.any,
WebMainFooteronKeyPress: PropTypes.any,
WebMainFooteronDrag: PropTypes.any,
WebMainFooteronMouseLeave: PropTypes.any,
WebMainFooteronMouseUp: PropTypes.any,
WebMainFooteronMouseDown: PropTypes.any,
WebMainFooteronKeyDown: PropTypes.any,
WebMainFooteronChange: PropTypes.any,
WebMainFooterondelay: PropTypes.any,
SpacingonClick: PropTypes.any,
SpacingonMouseEnter: PropTypes.any,
SpacingonMouseOver: PropTypes.any,
SpacingonKeyPress: PropTypes.any,
SpacingonDrag: PropTypes.any,
SpacingonMouseLeave: PropTypes.any,
SpacingonMouseUp: PropTypes.any,
SpacingonMouseDown: PropTypes.any,
SpacingonKeyDown: PropTypes.any,
SpacingonChange: PropTypes.any,
Spacingondelay: PropTypes.any,
XLinefourzerozeroonClick: PropTypes.any,
XLinefourzerozeroonMouseEnter: PropTypes.any,
XLinefourzerozeroonMouseOver: PropTypes.any,
XLinefourzerozeroonKeyPress: PropTypes.any,
XLinefourzerozeroonDrag: PropTypes.any,
XLinefourzerozeroonMouseLeave: PropTypes.any,
XLinefourzerozeroonMouseUp: PropTypes.any,
XLinefourzerozeroonMouseDown: PropTypes.any,
XLinefourzerozeroonKeyDown: PropTypes.any,
XLinefourzerozeroonChange: PropTypes.any,
XLinefourzerozeroondelay: PropTypes.any
}
export default VerificationEmail;