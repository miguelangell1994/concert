import React, { Suspense } from 'react';
import {
    Router,
    Switch,
    Route,
    useLocation,
    Link
  } from "react-router-dom";




import LoadingComponent from "Components/Loading";

import Logo from 'Components/Logo';
import RegistrationEmailTemplate from 'Components/RegistrationEmailTemplate';
import VerificationEmail from 'Components/VerificationEmail';
import ForgotPasswordEmail from 'Components/ForgotPasswordEmail';
import LoginEmailTemplate from 'Components/LoginEmailTemplate';




const Layout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
         <Route    component={Logo} path='/logo' exact={true} />
 <Route    component={RegistrationEmailTemplate} path='/concert2' exact={true} />
 <Route    component={VerificationEmail} path='/verificationemail' exact={true} />
 <Route    component={ForgotPasswordEmail} path='/forgotpasswordemail' exact={true} />
 <Route    component={LoginEmailTemplate} path='/' exact={true} />             
        </Switch>   
        
         
        
        
   
    )
};


const MobileLayout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
                     
        </Switch>
            
        
         
        
        
    )
};

export default Layout;
